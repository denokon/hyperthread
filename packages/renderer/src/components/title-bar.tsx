import {styled} from '../themes/theme';
import {Times, WindowMinimize, Home, SpaceShuttle} from 'styled-icons/fa-solid';
import * as React from 'react';
import {PlainButton} from './button';
import {uiInterface} from '../services/rpc-client';
import {history} from '../history';
import {ReactNode} from 'react';
import {StyledIconBase} from 'styled-icons/StyledIconBase';
import {zIndexTitleBar} from '../themes/z-indexes';

export const TitleBarHeight = '2.6rem';

const StyledTitleBar = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-items: center;
  font-size: 1.2rem;
  padding: 0;
  color: #d0d0d0;
  height: ${TitleBarHeight};
  -webkit-app-region: drag;
  transition: background-color .2s ease-in-out;
  background-color: rgba(255,255,255, .03);
  border-bottom: 1px solid rgba(255,255,255,0.1);
  line-height: 1;
  z-index: ${zIndexTitleBar};
  
  ${PlainButton} {
    -webkit-app-region: no-drag;
    
    height: 100%;
    width: 3.4rem;
    text-align: center;
    line-height: 1;
    font-size: 1.2rem;
    
    &:hover {
      background-color: rgba(255,255,255,.05);
      color: #eaeaea;
    }
  }
`;

const Title = styled.div`
  flex-grow: 1;
  padding-top: .1rem;
`;

const WindowButtons = styled.div`  
  height: 100%;

  & + & {
    margin-left: 1.5rem;  
  }
`;

const WindowIconButton = styled(PlainButton)`
  -webkit-app-region: no-drag;
  padding: 0 .6rem 0 1.2rem;
`;

export interface TitleBarProps
{
    children?: ReactNode;
    isStartPage?: boolean;
    extraButtons?: ReactNode;
}

export const TitleBar = ({children, isStartPage, extraButtons}: TitleBarProps) => <StyledTitleBar>
    <WindowIconButton onClick={() => history.push('/start')}>
        <SpaceShuttle />
    </WindowIconButton>
    <Title>{children}</Title>
    
    {extraButtons && <WindowButtons>
        {extraButtons}
    </WindowButtons>}
    <WindowButtons>
        <PlainButton onClick={() => uiInterface.minimize()}>
            <WindowMinimize />
        </PlainButton>
        <PlainButton onClick={() => uiInterface.close()}>
            <Times />
        </PlainButton>
    </WindowButtons>
</StyledTitleBar>;