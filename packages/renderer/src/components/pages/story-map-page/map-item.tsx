import {styled} from '../../../themes/theme';
import * as React from 'react';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {CSSProperties, forwardRef, Ref} from 'react';
import Dotdotdot from 'react-dotdotdot';

const StyledMapItem = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #464646;
  background-color: #2f2f2f;
  padding: .5rem;
  position: absolute;
  width: 12rem;
  height: 12rem;
  box-shadow: inset 0 0 .8rem #111;
  cursor: pointer;
`;

const PassageNameLabel = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-bottom: .5rem;
  font-size: 1.3rem;
`;

const PassageSnippet = styled.div`
  display: flex;
  min-height: 0;
  flex-grow: 1;
  font-size: 1rem;
  color: #bfbfbf;
  align-items: center;
  
  > div {
    overflow-wrap: break-word;
    min-width: 0;
    min-height: 0;
  }
`;

export interface MapItemProps
{
    passage: PassageInterface;
    onClick?(event: React.MouseEvent<HTMLDivElement>): void;
    style?: CSSProperties;
}

delete (Dotdotdot as any).propTypes;

function MapItemComponent({passage, style, forwardRef, onClick}: MapItemProps & { forwardRef: Ref<HTMLDivElement> })
{
    const snippetLength = 120;
    let snippet = passage.content.slice(0, snippetLength);
    
    if (snippetLength < passage.content.length)
        snippet += '...';
    
    return <StyledMapItem ref={forwardRef} style={style} onClick={onClick}>
        <PassageNameLabel>{passage.name}</PassageNameLabel>
        <PassageSnippet>
            <Dotdotdot clamp={5}>
                {snippet}
            </Dotdotdot>
        </PassageSnippet>
    </StyledMapItem>;
}

export const MapItem = forwardRef<HTMLDivElement, MapItemProps>((props, ref) =>
    <MapItemComponent {...props} forwardRef={ref} />
); 