export interface StateInterface
{
    passageName?: string;
    
    [key: string]: any;
}

export function createEmptyState(): StateInterface
{
    return {};
}

export interface StateContainer
{
    local: { [key: string]: any };
    global: { [key: string]: any };
}

export function createStateProxy(stateContainer: StateContainer): StateInterface
{
    // Based on: https://gist.github.com/soareschen/9b63a016174b6123abc073a2be068d48
    return new Proxy(stateContainer, {
        has(target, key) {
            // 'eval' for the eval() function and 'scriptContent', because that's the parameter name the contains the
            // code
            return key !== 'eval' && key !== 'scriptContent';
        },
        
        ownKeys(target): PropertyKey[] {
            return [
                ...Object.keys(target.global).map(key => '$' + key),
                ...Object.keys(target.local)
            ];
        },

        get(target, key: any) {
            // Symbol.unscopables has to return an object that contains properties excluded from being returned by the
            // with () {} statement
            if (key === Symbol.unscopables)
                return {};
            
            if (key in window)
                return (window as any)[key];

            if (key[0] === '$')
                return target.global[key.slice(1)];

            return target.local[key];
        },

        set(target, key: any, value) {
            if (key[0] === '$')
                target.global[key.slice(1)] = value;
            else
                target.local[key] = value;

            return true;
        }
    }) as unknown as StateInterface;
}