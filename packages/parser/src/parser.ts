import {Parser} from 'chevrotain';

const {
    Backtick,
    defaultTokens,
    LCurly,
    LCurlyBlockEnd,
    LCurlyBlockStart,
    ParagraphSeparator,
    PlainText,
    TripleBacktick,
    HardLineBreak,
    SoftLineBreak,
    EscapedCharacter
} = require('@hyperthread/parser/tokens/default-tokens');
const {
    ExpressionName, ExpressionParameter,
    ExpressionParameterSeparator,
    expressionTokens, RCurly
} = require('@hyperthread/parser/tokens/expression-tokens');
const {
    BlockCode, blockCodeTokens,
    ClosingBacktick,
    ClosingTripleBacktick,
    InlineCode,
    inlineCodeTokens
} = require('@hyperthread/parser/tokens/code-tokens');

export class HyperthreadParser extends Parser
{
    public passage = this.RULE('passage', () => {
        this.SUBRULE(this.content);
    });

    public content = this.RULE('content', () => {
        this.MANY(() => {
            this.OR([
                { ALT: () => this.CONSUME(PlainText) },
                { ALT: () => this.CONSUME(HardLineBreak) },
                { ALT: () => this.CONSUME(SoftLineBreak) },
                { ALT: () => this.CONSUME(ParagraphSeparator) },
                { ALT: () => this.CONSUME(EscapedCharacter) },
                { ALT: () => this.SUBRULE(this.expression) },
                { ALT: () => this.SUBRULE(this.blockExpression) },
                { ALT: () => this.SUBRULE(this.inlineCode) },
                { ALT: () => this.SUBRULE(this.blockCode) },
            ])
        })
    });
    
    private expression = this.RULE('expression', () => {
        this.CONSUME(LCurly);
        this.CONSUME(ExpressionName);
        this.MANY_SEP({
            SEP: ExpressionParameterSeparator,
            DEF: () => this.CONSUME(ExpressionParameter)
        });
        this.CONSUME(RCurly);
    });
    
    private blockExpression = this.RULE('blockExpression', () => {
        this.CONSUME(LCurlyBlockStart);
        this.CONSUME(ExpressionName);
        this.MANY_SEP({
            SEP: ExpressionParameterSeparator,
            DEF: () => this.CONSUME(ExpressionParameter)
        });
        this.CONSUME(RCurly);
        
        this.SUBRULE(this.content);
        
        this.CONSUME(LCurlyBlockEnd);
        this.OPTION(() => {
            this.CONSUME1(ExpressionName);
        });
        this.CONSUME1(RCurly);
    });
    
    private inlineCode = this.RULE('inlineCode', () => {
        this.CONSUME(Backtick);
        this.CONSUME(InlineCode);
        this.CONSUME(ClosingBacktick);
    });
    
    private blockCode = this.RULE(`blockCode`, () => {
        this.CONSUME(TripleBacktick);
        this.CONSUME(BlockCode);
        this.CONSUME(ClosingTripleBacktick);
    });
    
    public constructor()
    {
        super([
            ...defaultTokens,
            ...expressionTokens,
            ...inlineCodeTokens,
            ...blockCodeTokens
        ]);
        
        this.performSelfAnalysis();
    }
}

export const parser = new HyperthreadParser();