import {BaseComponent, bind} from '@epicentric/utilbelt-react';
import {default as React, Fragment, ReactNode} from 'react';
import {parseToCst} from '@hyperthread/parser';
import {styled} from '../../themes/theme';
import Editor from 'react-simple-code-editor';
import {InputPlus} from '@epicentric/input-plus';
import {IRecognitionException, IToken} from 'chevrotain';
import produce from 'immer';
import Highlight, {defaultProps, PrismTheme} from 'prism-react-renderer';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {PlainButton, RoundButton} from '../button';
import {Play, Plus} from 'styled-icons/fa-solid';
import {Delete} from 'styled-icons/material';
// import prismTheme from 'prism-react-renderer/themes/duotoneDark';

// Created with: https://github.com/FormidableLabs/prism-react-renderer/tree/master/tools/themeFromVsCode
const prismTheme: PrismTheme  = {
    "plain": {
        "color": "#abb2bf",
        "backgroundColor": "#282c34"
    },
    "styles": [
        {
            "types": [
                "constant",
                "deleted",
                "tag"
            ],
            "style": {
                "color": "rgb(224, 108, 117)"
            }
        },
        {
            "types": [
                "punctuation"
            ],
            "style": {
                "color": "rgb(171, 178, 191)"
            }
        },
        {
            "types": [
                "variable",
                "builtin",
                "changed",
                "namespace",
                "class-name"
            ],
            "style": {
                "color": "rgb(229, 192, 123)"
            }
        },
        {
            "types": [
                "operator",
                "symbol"
            ],
            "style": {
                "color": "rgb(86, 182, 194)"
            }
        },
        {
            "types": [
                "inserted",
                "char"
            ],
            "style": {
                "color": "rgb(152, 195, 121)"
            }
        },
        {
            "types": [
                "attr-name",
                "comment"
            ],
            "style": {
                "fontStyle": "italic"
            }
        },
        {
            "types": [
                "function",
                "string"
            ],
            "style": {
                "color": "rgb(97, 175, 239)"
            }
        },
        {
            "types": [
                "keyword",
                "selector"
            ],
            "style": {
                "color": "rgb(198, 120, 221)"
            }
        },
        {
            "types": [
                "number"
            ],
            "style": {
                "color": "rgb(209, 154, 102)"
            }
        }
    ]
};

function getTokenColor(name: string)
{
    const entry = prismTheme.styles.find(style => style.types.includes(name));
    
    return entry ? entry.style.color : 'pink';
}

const punctuationColor = getTokenColor('punctuation');
const functionColor = getTokenColor('function');
const backgroundColor = prismTheme.plain.backgroundColor;

export const StyledPassageEditor = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
  flex-grow: 1;
  background-color: ${backgroundColor};
  
    &::-webkit-scrollbar {
      width: 4px;
      height: 4px;
    }
    &::-webkit-scrollbar-track {
      background: none;
      border-width: 0;
    }
    &::-webkit-scrollbar-thumb {
      transition: background-color 250ms ease;
      background-color: transparent;
      border-radius: .2rem;
    }
    &::-webkit-scrollbar-button {
      display: none;
    }
    &::-webkit-scrollbar-track-piece {
      display: none;
    }
    &::-webkit-scrollbar-corner {
      display: none;
    }
    &::-webkit-resizer {
      display: none;
    }
    &:hover::-webkit-scrollbar {
    }
    &:hover::-webkit-scrollbar-thumb {
      background-color: rgba(255,255,255,.16);
    }
`;

const PassageTextArea = styled(Editor)`
  flex-grow: 1;
  font-family: Consolas;
  margin: 0 2rem 1rem;
  
  textarea:focus {
    outline: 0;
  }
  
  textarea::selection  {
    color: transparent;
    background-color: rgba(71, 179, 255, 0.52);
  }
`;

const PassageTitle = styled(InputPlus)`
  border-bottom: 1px solid rgba(255,255,255, 0.15);
  display: block;
  font-size: 2rem;
  color: #a8a8a8;
  transition: color .2s ease-in-out, border-color .2s ease-in-out;
  margin: 1rem 2rem 1.6rem;
  
  &:focus {
    outline: 0;
  }
  
  &:focus, &:hover {
    color: inherit;
    border-bottom-color: rgba(255,255,255, 0.3);
  }
`;

const ErrorList = styled.div`
  border-top: 1px solid #464646;
  background-color: #2f2f2f;
  color: red;
  padding: 1.2rem 1.0rem;
`;

const PassageTitleBar = styled.div`
  display: flex;
  align-items: center;
  padding: 1rem 2rem 1.6rem;
  
  ${PassageTitle} {
    flex-grow: 1;
    margin: 0 1rem;    
  }

  ${RoundButton} {
    font-size: 1.0rem;
  }
  
  ${PlainButton} {
    font-size: 1.8rem;
  }
  
  ${RoundButton} > svg {
    position: relative;
    left: .1em;
    bottom: .1em;
  }
`;

export interface PassageEditorProps
{
    passage: PassageInterface;
    onChange: (passage: PassageInterface) => void;
    onDelete?: () => void;
}

export class PassageEditor extends BaseComponent<PassageEditorProps>
{
    public render(): ReactNode
    {
        const passage = this.props.passage;
        
        const textContent = passage.content;
        const parsingResult = parseToCst(textContent);
        const resultTokens = parsingResult.lexingResult.tokens; // Closure optimization
        
        console.log(parsingResult);

        return <StyledPassageEditor>
            <PassageTitleBar>
                <RoundButton
                    outline={!passage.isStart}
                    buttonStyle={passage.isStart ? 'success' : 'default'}
                    onClick={this.onSetStart}
                >
                    <Play />
                </RoundButton>
                <PassageTitle
                    value={passage.name}
                    onChange={this.onNameChange}
                />
                <PlainButton onClick={this.onDelete}>
                    <Delete />
                </PlainButton>
            </PassageTitleBar>
            <PassageTextArea
                value={passage.content}
                onValueChange={this.onContentChange}
                highlight={() => this.onHighlightEditor(textContent, resultTokens)}
                tabSize={4}
            />
            {0 < parsingResult.parseErrors.length + parsingResult.lexingResult.errors.length && <ErrorList>
                {parsingResult.parseErrors.map((error, index) => <div key={'P' + index}>
                    P ({this.getParseErrorPosition(error)}): {error.message}
                </div>)}
                {parsingResult.lexingResult.errors.map((error, index) => <div key={'T' + index}>
                    T ({error.line}, {error.column}): {error.message}
                </div>)}
            </ErrorList>}
        </StyledPassageEditor>;
    }

    @bind
    private onNameChange(value: string)
    {        
        this.props.onChange(produce(this.props.passage, draft => {
            draft.name = value;
        }));
    }

    @bind
    private onContentChange(value: string)
    {
        this.props.onChange(produce(this.props.passage, draft => {
            draft.content = value;
        }));
    }

    @bind
    private onSetStart()
    {
        if (this.props.passage.isStart)
            return;
        
        this.props.onChange(produce(this.props.passage, draft => {
            draft.isStart = true;
        }));
    }

    @bind
    private onDelete()
    {
        if (!confirm('Are you sure?'))
            return;
        
        this.props.onDelete && this.props.onDelete();
    }

    @bind
    private onHighlightEditor(value: string, tokens: IToken[]): React.ReactNode
    {
        const syntaxTokens: React.ReactNode[] = [];

        for (let index = 0; index < tokens.length; index++)
        {
            const prevToken: IToken|undefined = tokens[index - 1];
            const token = tokens[index];

            if (prevToken && prevToken.endOffset && (prevToken.endOffset + 1 !== token.startOffset))
            {
                syntaxTokens.push(<Fragment key={syntaxTokens.length}>
                    {value.substring(prevToken.endOffset + 1, token.startOffset)}
                </Fragment>);
            }

            switch (token.tokenType && token.tokenType.name)
            {
                case 'LCurly':
                case 'RCurly':
                case 'LCurlyBlockStart':
                case 'LCurlyBlockEnd':
                case 'ExpressionParameterSeparator':
                case 'Backtick':
                case 'ClosingBacktick':
                case 'TripleBacktick':
                case 'ClosingTripleBacktick':
                    syntaxTokens.push(<span key={syntaxTokens.length} style={{color: punctuationColor}}>{token.image}</span>);
                    break;
                case 'ExpressionName':
                    syntaxTokens.push(<span key={syntaxTokens.length} style={{color: functionColor}}>{token.image}</span>);
                    break;
                case 'ExpressionParameter':
                    // syntaxTokens.push(<span key={syntaxTokens.length} style={{color: 'rgb(229, 187, 128)'}}>{token.image}</span>);
                    syntaxTokens.push(this.getRenderedCode(token.image, syntaxTokens.length));
                    break;
                case 'InlineCode':
                    // syntaxTokens.push(<span key={syntaxTokens.length} style={{color: 'rgb(229, 187, 128)'}}>{token.image}</span>);
                    syntaxTokens.push(this.getRenderedCode(token.image, syntaxTokens.length));
                    break;
                case 'BlockCode':
                    // syntaxTokens.push(<div key={syntaxTokens.length} style={{color: 'rgb(229, 187, 128)', display: 'inline'}}>{token.image}</div>);
                    syntaxTokens.push(this.getRenderedCode(token.image, syntaxTokens.length));
                    break;
                default:
                    syntaxTokens.push(<Fragment key={syntaxTokens.length}>{token.image}</Fragment>);
                    break;
            }
        }

        const lastToken: IToken|undefined = tokens[tokens.length - 1];

        if (lastToken && lastToken.endOffset && lastToken.endOffset + 1 !== value.length)
        {
            syntaxTokens.push(<Fragment key={syntaxTokens.length}>
                {value.substring(lastToken.endOffset + 1)}
            </Fragment>);
        }

        return syntaxTokens;
    }
    
    private getRenderedCode(code: string, key: number)
    {        
        return <Highlight
            {...defaultProps}
            key={key}
            language="javascript"
            code={code}
            theme={prismTheme}
        >
            {({className, style, tokens, getLineProps, getTokenProps}) => (
                <div className={className} style={this.getPrismStyle(style)}>
                    {tokens.map((line, i, arr) => {
                        const lineProps = 0 === i || arr.length - 1 === i
                            ? getLineProps({line, key: i, style: { display:  'inline' }})
                            : getLineProps({line, key: i});
                        
                        return <div {...lineProps}>
                            {line.map((token, key) => (
                                <span {...getTokenProps({token, key})} />
                            ))}
                        </div>
                    })}
                </div>
            )}
        </Highlight>;
    }
    
    private getPrismStyle(style: any)
    {
        return { ...style, display: 'inline', background: 'none' };
    }

    private getParseErrorPosition(error: IRecognitionException & { previousToken?: IToken }): string
    {
        const line = undefined === error.token.startLine || Number.isNaN(error.token.startLine)
            ? error.previousToken && error.previousToken.startLine
            : error.token.startLine;

        const column = undefined === error.token.startColumn || Number.isNaN(error.token.startColumn)
            ? error.previousToken && error.previousToken.startColumn
            : error.token.startColumn;
        
        return `${line}, ${column}`;
    }
}