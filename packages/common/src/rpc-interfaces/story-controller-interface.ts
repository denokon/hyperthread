import {ParsedStoryInterface, StoryInterface} from '../models/story-interface';
import {Observable} from 'rxjs';

export const storyControllerPromiseFunctions = [ 'create', 'openPath', 'openId', 'save', 'createFilePath' ];

export interface CreateStoryParams
{
    name: string;
    path: string;
    createDirectory: boolean;
}

export interface StoryControllerInterface
{
    create(params: CreateStoryParams): Promise<StoryInterface>;
    openPath(path: string): Promise<StoryInterface>;
    openId(id: string): Promise<StoryInterface>;
    save(story: StoryInterface): Promise<void>;
    watch(path: string): Observable<ParsedStoryInterface>;
    createFilePath(projectPath: string, storyName: string, createDirectory: boolean): Promise<string>;
    play(story: StoryInterface): Promise<void>;
}