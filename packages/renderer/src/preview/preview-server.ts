import {MsgpackRpcEncoder, rpcCallable, RpcClient, RpcHandler, RpcRequest} from '@epicentric/rxpc';
import {PostmessageTransport} from '@epicentric/rxpc-transport-postmessage';
import {NodeIdGenerator} from '@epicentric/binary-id';
import {PreviewClientInterface} from '@hyperthread/common/rpc-interfaces/preview-client-interface';
import {ParsedStoryInterface} from '@hyperthread/common/models/story-interface';
import {Observable, ReplaySubject} from 'rxjs';
import {ServerInterface} from '@hyperthread/common/rpc-interfaces/server-interface';
import {PreviewServerInterface} from '@hyperthread/common/rpc-interfaces/preview-server-interface';

const rpcHandler = new RpcHandler();
const rpcHook = rpcHandler.hooks.hook;

const idGenerator = new NodeIdGenerator();
const encoder = new MsgpackRpcEncoder();

export class PreviewServer implements ServerInterface<PreviewServerInterface>
{    
    public readonly controller: PreviewClientInterface;
    
    public historyIndex: number = 0;
    public historyLength: number = 0;
    
    private readonly client: RpcClient;
    private readonly transport: PostmessageTransport;
    private readonly story$: ReplaySubject<ParsedStoryInterface> = new ReplaySubject(1);
    
    public constructor(targetWindow: Window)
    {
        rpcHandler.registerClass('previewServer', this);
        
        this.transport = new PostmessageTransport({targetWindow, currentWindow: window});

        const client = new RpcClient({
            idGenerator,
            encoder,
            transport: this.transport,
            pingEnabled: false,
            reconnectionEnabled: false
        });

        client.message.subscribe(message => {
            console.log('rpc message', message);
            rpcHandler.handle(client, message, {});
        });

        client.connect();

        this.client = client;
        this.controller = rpcHandler.createProxyClass<PreviewClientInterface>(client, 'previewClient', () => ({}));
        
        this.controller.currentIndex().subscribe(info => {
            this.historyIndex = info.index;
            this.historyLength = info.length;
        });
    }
    
    @rpcCallable()
    public getStory(request: RpcRequest): Observable<ParsedStoryInterface>
    {
        return this.story$;
    }
    
    public setStory(story: ParsedStoryInterface): void
    {
        this.story$.next(story);
    }
    
    public close(sendCloseMessage: boolean): void
    {
        if (!sendCloseMessage)
        {
            // Don't send out a disconnect message in the middle of another connection. It would trip it up.
            this.transport.sendingEnabled = false;
        }
        
        this.transport.close();
    }
}