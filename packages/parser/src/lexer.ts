import {Lexer} from 'chevrotain';
import {expressionTokens} from './tokens/expression-tokens';
import {defaultTokens} from './tokens/default-tokens';
import {blockCodeMode, defaultMode, expressionMode, inlineCodeMode} from './tokens/token-modes';
import {blockCodeTokens, inlineCodeTokens} from '@hyperthread/parser/tokens/code-tokens';

export const lexer = new Lexer({
    modes: {
        [defaultMode]: defaultTokens,
        [expressionMode]: expressionTokens,
        [inlineCodeMode]: inlineCodeTokens,
        [blockCodeMode]: blockCodeTokens
    },
    defaultMode: defaultMode
});