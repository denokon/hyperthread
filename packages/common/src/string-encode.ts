import {fromByteArray, toByteArray} from 'base64-js';

const encoder = new TextEncoder();
const decoder = new TextDecoder();

export function stringEncode(input: string): string
{
    const bytes = encoder.encode(input);    
    const encodedString = fromByteArray(bytes);
    
    return encodedString.replace(/[+\/=]/g, match => {
        switch (match)
        {
            case '+':
                return '-';
            case '/':
                return '_';
            case '=':
                return '';
            default:
                return match;
        }
    });
}

export function stringDecode(input: string): string
{
    let actualBase64String = input.replace(/[+\/]/g, match => {
        switch (match)
        {
            case '-':
                return '+';
            case '_':
                return '_';
            default:
                return match;
        }
    });
    
    if (actualBase64String.length % 4 !== 0)
        actualBase64String = actualBase64String.padEnd(Math.ceil(actualBase64String.length / 4) * 4, '=');
    
    const bytes = toByteArray(actualBase64String);
    return decoder.decode(bytes);
}