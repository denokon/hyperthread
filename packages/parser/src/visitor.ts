import {parser} from './parser';
import {
    EscapedCharacter,
    HardLineBreak,
    ParagraphSeparator,
    PlainText,
    SoftLineBreak
} from '@hyperthread/parser/tokens/default-tokens';
import {
    AstNodeType,
    CodeAstNode,
    ContentAstNode,
    ExpressionAstNode,
    PassageAstNode
} from '@hyperthread/parser/ast-nodes';

const BaseVisitor = parser.getBaseCstVisitorConstructor();

class HyperthreadVisitor extends BaseVisitor
{
    public constructor()
    {
        super();

        // The "validateVisitor" method is a helper utility which performs static analysis
        // to detect missing or redundant visitor methods
        this.validateVisitor();
    }

    public passage(ctx: any): PassageAstNode
    {
        const content = this.visit(ctx.content);
        
        return {
            type: AstNodeType.Passage,
            content
        };
    }
    
    public content(ctx: any): ContentAstNode
    {
        const sortedTokens = this.getSortedTokens(ctx);
        const children: any[] = [];
        
        for (const token of sortedTokens)
        {            
            switch (token.tokenType)
            {
                case PlainText:
                    children.push({
                        type: AstNodeType.PlainText,
                        content: token.image
                    });
                    break;
                case HardLineBreak:
                    children.push({
                        type: AstNodeType.HardLineBreak
                    });
                    break;
                case SoftLineBreak:
                    children.push({
                        type: AstNodeType.PlainText,
                        content: '\n'
                    });
                    break;
                case ParagraphSeparator:
                    children.push({
                        type: AstNodeType.ParagraphSeparator
                    });
                    break;
                case EscapedCharacter:
                    children.push({
                        type: AstNodeType.PlainText,
                        content: token.image[1] // Get the escaped character
                    });
                    break;
                default: // expression                    
                    children.push(this.visit(token));
                    break;
            }
        }
        
        return {
            type: AstNodeType.Content,
            children
        };
    }
    
    public expression(ctx: any): ExpressionAstNode
    {
        return {
            type: AstNodeType.Expression,
            name: ctx.ExpressionName[0].image,
            parameters: this.sortTokens(ctx.ExpressionParameter).map(param => param.image)
        };
    }

    public blockExpression(ctx: any): ExpressionAstNode
    {
        const startExpressionName = ctx.ExpressionName[0].image;
        const endExpressionName = ctx.ExpressionName[1] && ctx.ExpressionName[1].image;
        
        if (endExpressionName && endExpressionName !== startExpressionName)
            throw new Error(`Block expression name matching error: '${startExpressionName}' !== '${endExpressionName}'`);
        
        return {
            type: AstNodeType.Expression,
            name: startExpressionName,
            parameters: this.sortTokens(ctx.ExpressionParameter).map(param => param.image),
            content: this.visit(ctx.content)
        };
    }
    
    public inlineCode(ctx: any): CodeAstNode
    {
        return {
            type: AstNodeType.Code,
            content: ctx.InlineCode[0].image,
            isBlock: false
        };
    }
    
    public blockCode(ctx: any): CodeAstNode
    {
        return {
            type: AstNodeType.Code,
            content: ctx.BlockCode[0].image,
            isBlock: true
        };
    }

    private getTokens(ctx: any): any[]
    {
        const tokens: any[] = [];

        for (const currentTokens of Object.values(ctx) as any[][])
        {
            if (!currentTokens.length)
                continue;
            
            tokens.push(...currentTokens);
        }

        return tokens;
    }

    private sortTokens(tokens: any[])
    {        
        const tokensWithOffsets: [number, any][] = [];
        
        for (const token of tokens || [])
        {
            const startOffset = this.getStartOffset(token);
            
            if (startOffset < 0)
                continue;

            tokensWithOffsets.push([startOffset, token]);
        }
        
        tokensWithOffsets.sort((a, b) => a[0] - b[0]);
        
        return tokensWithOffsets.map(tuple => tuple[1]);
    }

    private getSortedTokens(ctx: any): any[]
    {
        const tokens = this.getTokens(ctx);
        return this.sortTokens(tokens);
    }

    private getStartOffset(token: any): number
    {
        if ('startOffset' in token)
            return token.startOffset;

        if (token.children)
        {
            // This is pretty suboptimal as it resorts on every comparison, but it works
            const sortedTokens = this.getSortedTokens(token.children);
            
            if (sortedTokens.length)
                return sortedTokens[0].startOffset;
        }

        return -1;
    } 
}

export const visitor = new HyperthreadVisitor();