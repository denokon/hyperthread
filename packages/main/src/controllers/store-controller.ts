import {from, Observable} from 'rxjs';
import {DeleteParams, GetParams, SetParams, StoreInterface} from '@hyperthread/common/rpc-interfaces/store-interface';
import {rpcCallable, RpcRequest} from '@epicentric/rxpc';
import {store} from '../services/store';
import {ServerInterface} from '@hyperthread/common/rpc-interfaces/server-interface';

export class StoreController implements ServerInterface<StoreInterface>
{
    @rpcCallable()
    public delete(request: RpcRequest, params: DeleteParams): Observable<void>
    {
        return from(store.delete(params.key));
    }

    @rpcCallable()
    public get<T>(request: RpcRequest, params: GetParams<T>): Observable<T|undefined>
    {
        return store.get(params.key, params.defaultValue);
    }

    @rpcCallable()
    public set<T>(request: RpcRequest, params: SetParams<T>): Observable<void>
    {
        return from(store.set(params.key, params.value));
    }
}

export const storeController = new StoreController;