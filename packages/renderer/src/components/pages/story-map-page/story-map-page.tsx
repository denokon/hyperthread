import {RouteComponentProps} from 'react-router';
import * as React from 'react';
import {useCallback, useEffect, useMemo} from 'react';
import {useObservable} from 'rxjs-hooks';
import {Page} from '../../page';
import {TitleBar} from '../../title-bar';
import {MapArea} from './map-area';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {StoryMapDragLayer} from './story-map-drag-layer';
import {history} from '../../../history';
import {EditorModal} from './editor-modal';
import {styled} from '../../../themes/theme';
import {RoundButton} from '../../button';
import {Play, Plus} from 'styled-icons/fa-solid';
import {storyInterface} from '../../../services/rpc-client';
import {LoadedStory} from '../../../loaded-story';

const BottomButtonsContainer = styled.div`
  position: absolute;
  right: 2rem;
  bottom: 2rem;
  font-size: 1.8rem;
  
  ${RoundButton} + ${RoundButton} {
    margin-left: 1.5rem;
  }
`;

interface StoryMapPageProps extends RouteComponentProps<{ storyId: string, passageId?: string }>
{

}

export function StoryMapPage(props: StoryMapPageProps)
{
    const storyId = props.match.params.storyId;
    const loadedStory = useMemo(() => new LoadedStory(), []);
    
    useEffect(() => {
        loadedStory.loadStory(storyId);
        
        return () => {
            loadedStory.dispose();
        };
    }, [storyId]);
    
    
    const story = useObservable(() => loadedStory.change$, loadedStory.story);
    
    const onPassageChange = useCallback((passage: PassageInterface) => {
        loadedStory.updatePassage(passage);
    }, [loadedStory]);
    
    const onPassageClick = useCallback((passage: PassageInterface) => {
        history.push(`/stories/${storyId}/passages/${passage.name}`);
    }, [storyId]);
    
    const onAddPassage = useCallback(() => {
        loadedStory.addPassage()
    }, [loadedStory]);
    
    const onPlay = useCallback(() => {
        storyInterface.play(loadedStory.story);
    }, [loadedStory]);
    
    return <Page>
        <TitleBar>{story.name} {loadedStory.hasUnsavedChanges && '*'}</TitleBar>
        <StoryMapDragLayer />
        <MapArea
            passages={story.passages}
            story={story as any} // TODO fix this
            onPassageChange={onPassageChange}
            onPassageClick={onPassageClick}
        />
        <EditorModal
            story={story}
            loadedStory={loadedStory}
            storyId={storyId}
            passageId={props.match.params.passageId}
        />
        <BottomButtonsContainer>
            <RoundButton onClick={onAddPassage} buttonStyle="success"><Plus/></RoundButton>
            <RoundButton onClick={onPlay}><Play /></RoundButton>
        </BottomButtonsContainer>
    </Page>;
}