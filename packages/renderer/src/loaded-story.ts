import {createEmptyStory, StoryInterface} from '@hyperthread/common/models/story-interface';
import {storyInterface} from './services/rpc-client';
import {Subject} from 'rxjs';
import {throttle} from 'lodash-es';
import produce from 'immer';
import {createEmptyPassage, generatePassageId, PassageInterface} from '@hyperthread/common/models/passage-interface';

export class LoadedStory
{
    public change$: Subject<StoryInterface> = new Subject;
    
    public story: StoryInterface = createEmptyStory();
    public hasUnsavedChanges = false;

    private readonly throttledUpdateStory = throttle(this.persistStory, 500, { leading: true, trailing: true });
    private isSaving = false;
    private isDisposed = false;
    
    public async loadStory(id: string)
    {
        const story = await storyInterface.openId(id);
        
        this.setStory(story);
    }
    
    public updateStory(recipe: (draft: StoryInterface) => void): void
    {
        this.setStory(produce(this.story, recipe));
    }
    
    public updatePassage(passage: PassageInterface): void
    {
        const passageIndex = this.story.passages.findIndex(p => p.id === passage.id);
        
        if (passageIndex < 0)
            return;
        
        this.updateStory(draft => {
            draft.passages[passageIndex] = passage;
        });
    }
    
    public deletePassage(passage: PassageInterface)
    {
        // Remove passage from the passages array
        const passageIndex = this.story.passages.findIndex(current => current.name === passage.name);
        
        if (passageIndex < 0)
            return;
        
        this.updateStory(draft => {
            draft.passages.splice(passageIndex, 1);
        });
    }
    
    public addPassage(): void
    {
        const passages = this.story.passages;
        let newPassageIndex = 1;

        while (passages.find(passage => passage.name === `New passage ${newPassageIndex}`))
        {
            newPassageIndex++;
        }

        const newPassage: PassageInterface = createEmptyPassage();
        newPassage.id = generatePassageId();
        newPassage.name = `New passage ${newPassageIndex}`;
        
        this.updateStory(draft => {
            draft.passages.push(newPassage);
        });
    }
    
    public setStory(story: StoryInterface): void
    {
        if (this.story === story)
            return;
        
        this.story = story;
        this.hasUnsavedChanges = true;
        this.change$.next(story);

        this.throttledUpdateStory();
    }
    
    public dispose(): void
    {
        this.dispose();
        this.change$.complete();
    }
    
    protected async persistStory(): Promise<void>
    {
        if (this.isSaving || this.isDisposed)
            return;

        try
        {
            this.isSaving = true;
            await storyInterface.save(this.story);
            this.hasUnsavedChanges = false;
            this.change$.next(this.story);
        }
        finally
        {
            this.isSaving = false;
        }
    }
}