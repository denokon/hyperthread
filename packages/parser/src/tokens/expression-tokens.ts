import {createToken} from 'chevrotain';
import {LCurly, LCurlyBlockEnd, LCurlyBlockStart} from './default-tokens';
import {Whitespace} from '@hyperthread/parser/tokens/generic-tokens';

export const RCurly = createToken({
    name: 'RCurly',
    pattern: /}/,
    pop_mode: true
});

export const ExpressionName = createToken({
    name: 'ExpressionName',
    pattern: (text, offset, tokens) => {
        if (!tokens || 0 === tokens.length)
            return null;

        const lastTokenType = tokens[tokens.length - 1].tokenType;

        if (lastTokenType !== LCurly && lastTokenType !== LCurlyBlockStart && lastTokenType !== LCurlyBlockEnd)
            return null;

        return /^[a-zA-Z]+/.exec(text.substring(offset));
    },
    line_breaks: false
});

const expressionParameterRegexp = /\s*([,{}])/g;

function getExpressionParameterRange(text: string, offset: number)
{
    expressionParameterRegexp.lastIndex = offset;

    let currentLevel = 0;
    let currentMatch: RegExpExecArray | null;

    // Find the next ",", "{" or "}" character
    // noinspection JSAssignmentUsedAsCondition
    while (currentMatch = expressionParameterRegexp.exec(text))
    {
        switch (currentMatch[1])
        {
            case ',':
                if (0 === currentLevel)
                    return currentMatch.index;

                break;
            case '{':
                currentLevel++;

                break;
            case '}':
                if (0 === currentLevel)
                    return currentMatch.index;

                currentLevel--;
                break;
        }
    }

    return offset;
}

export const ExpressionParameter = createToken({
    name: 'ExpressionParameter',
    pattern: (text, offset) => {
        if (' \t\n\r\v'.includes(text[offset]))
            return null; // Do not match whitespace

        const end = Math.max(offset, getExpressionParameterRange(text, offset));

        if (offset === end)
            return null; // We did not find a match

        return [text.substring(offset, end)] as RegExpExecArray;
    },
    line_breaks: true
});

export const ExpressionParameterSeparator = createToken({
    name: 'ExpressionParameterSeparator',
    pattern: /,/
});

export const expressionTokens = [
    Whitespace, // Whitespace in front for performance reasons
    RCurly,
    ExpressionName,
    ExpressionParameter,
    ExpressionParameterSeparator,
];