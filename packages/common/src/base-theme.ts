import reboot from 'styled-reboot';
import {Theme} from '@hyperthread/renderer/src/themes/theme';
import * as baseStyled from 'styled-components';
import {ThemedStyledComponentsModule} from 'styled-components';

// So much hacks! Basically we extract the first argument from the function reboot()
// Based on: https://github.com/Microsoft/TypeScript/issues/26019#issuecomment-408520542
type Parameters<T> = T extends (... args: infer T) => any ? T : never;
export type RebootOptions = Exclude<Parameters<typeof reboot>[0], undefined>;

export interface BaseTheme extends RebootOptions
{
}

const rebootStyle = reboot({
    bodyBg: ({theme}) => theme.bodyBg,
    bodyColor: ({theme}) => theme.bodyColor,
    fontFamilyBase: ({theme}) => theme.fontFamilyBase,
} as { [P in keyof RebootOptions]: (props: { theme: Theme }) => string } as any);

const { css } = baseStyled as ThemedStyledComponentsModule<Theme>;

export const baseGlobalStyle = css`
  ${rebootStyle}

  html {
    font-size: .625rem;
  }
  
  body {
    font-size: 1.4rem;
  }
`;