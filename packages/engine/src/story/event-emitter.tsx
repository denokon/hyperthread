export type EventEmitterListener = (...args: any[]) => Promise<any> | void;

export class EventEmitter
{
    private listeners: Map<string | symbol, Set<EventEmitterListener>> = new Map;

    public async emit(eventName: string, ...args: any[]): Promise<void>
    {
        const listeners = this.listeners.get(eventName);

        if (!listeners)
            return;

        for (const listener of listeners)
        {
            await listener(...args);
        }
    }

    public on(eventName: string | symbol, listener: EventEmitterListener): void
    {
        let listeners = this.listeners.get(eventName);

        if (!listeners)
        {
            listeners = new Set;
            this.listeners.set(eventName, listeners);
        }

        listeners.add(listener);
    }

    public off(eventName: string | symbol, listener: EventEmitterListener): void
    {
        const listeners = this.listeners.get(eventName);

        if (!listeners)
            return;

        listeners.delete(listener);
    }

    public addListener(eventName: string | symbol, listener: EventEmitterListener): void
    {
        return this.on(eventName, listener);
    }

    public removeListener(eventName: string | symbol, listener: EventEmitterListener): void
    {
        return this.off(eventName, listener);
    }
}