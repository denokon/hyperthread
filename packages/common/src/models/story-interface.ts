import {PassageAstNode} from '@hyperthread/parser/ast-nodes';
import {createEmptyPassage, PassageInterface} from '@hyperthread/common/models/passage-interface';
import nanoid from 'nanoid';

export interface StoryInterface
{
    id: string;
    path: string;
    name: string;
    passages: PassageInterface[];
    startPassageId: string;
    openPassageId: string|null;
}

export interface ParsedStoryInterface
{
    id: string;
    name: string;
    startPassageId: string;
    passages: ParsedStoryPassage[];
}

export interface ParsedStoryPassage
{
    name: string;
    node: PassageAstNode;
    isStart: boolean;
}

export function generateStoryId()
{
    return nanoid();
}

export function createEmptyStory(): StoryInterface
{
    const startPassage = createEmptyPassage();
    startPassage.name = 'Start passage';
    
    return {
        id: '',
        name: '',
        passages: [ startPassage ],
        path: '',
        startPassageId: startPassage.id,
        openPassageId: null
    };
}

