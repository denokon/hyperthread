import React, {ReactNode, useCallback, useEffect, useRef, useState} from 'react';
import {styled} from '../../../themes/theme';
import {storeInterface} from '../../../services/rpc-client';
import {StoryInterface} from '@hyperthread/common/models/story-interface';
import {FolderOpen, Plus} from 'styled-icons/fa-solid';
import {Main, Page} from '../../page';
import {TitleBar} from '../../title-bar';
import {RecentStories} from './recent-stories';
import {Fab, FabContainer, PopupModal, ModalBackground, ModalBox} from '@denokon/comp-lib-3';
import {useObservable} from 'rxjs-hooks';
import {CreateStory} from '../../create-story';
import {transitions} from 'polished';

const StyledStartPage = styled(Page)`
  display: flex;
`;

export function StartPage()
{
    const [createModalIsOpen, setCreateModalIsOpen] = useState(false);
    
    const recentStories = useObservable(() => storeInterface.get<StoryInterface[]>({
        key: 'recentStories'
    }), []) || [];
    
    const onCreateClick = useCallback(() => setCreateModalIsOpen(true), []);
    
    return <>
        <StyledStartPage>
            <TitleBar>Recent stories</TitleBar>
            <Main>
                <RecentStories stories={recentStories} />
                <FabContainer>
                    <Fab title="Open story"><FolderOpen /></Fab>
                    <Fab title="Create new story" color="success" onClick={onCreateClick}><Plus /></Fab>
                </FabContainer>
            </Main>
        </StyledStartPage>
        { createModalIsOpen
            ? <SimpleModal onClosed={() => setCreateModalIsOpen(false)}>
                <CreateStory />
            </SimpleModal>
            : null }
    </>;
}

function SimpleModal({children, onClosed}: { children?: ReactNode, onClosed: () => void })
{
    const [isOpen, setIsOpen] = useState(false);
    
    const onBackgroundClick = useCallback(() => {
        setIsOpen(false);
    }, []);
    
    const onTransitionEnd = useCallback((event: React.TransitionEvent<HTMLDivElement>) => {
        if (!isOpen)
            onClosed();
    }, [isOpen]);
    
    useEffect(() => {
        setIsOpen(true);
    }, []);
    
    return <>
        <ModalBackground isOpen={isOpen} onClick={onBackgroundClick} />
        <SimpleModalBox isOpen={isOpen} onTransitionEnd={onTransitionEnd}>
            {children}
        </SimpleModalBox>
    </>;
}

const SimpleModalBox = styled(ModalBox)<{ isOpen?: boolean }>`
  position: fixed;
  left: 50%;
  top: 5rem;
  
  opacity: ${props => props.isOpen ? 1 : 0};
  transform: ${props => `translateX(-50%) ` + (props.isOpen ? `scale(1)` : `scale(.5)`)};
  ${transitions(['opacity', 'transform'], '.25s ease-in-out')};
`;