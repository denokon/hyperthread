import {Theme} from './theme';

export const defaultTheme: Theme = {
    // App
    bodyBg: '#222222',
    bodyColor: '#ebebeb',
    fontFamilyBase: 'Inter'
};