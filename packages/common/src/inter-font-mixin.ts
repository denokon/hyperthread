/// <reference path="./types/fonts.d.ts" />
import {css} from 'styled-components';

import interRegular from '@hyperthread/common/fonts/inter/Inter-Regular.woff2';
import interMedium from '@hyperthread/common/fonts/inter/Inter-Medium.woff2';
import interBold from '@hyperthread/common/fonts/inter/Inter-Bold.woff2';

export const interFontMixin = css`
  @font-face {
    font-family: 'Inter';
    font-style:  normal;
    font-weight: 400;
    src: url("${interRegular}") format("woff2");
  }
  
  @font-face {
    font-family: 'Inter';
    font-style:  normal;
    font-weight: 500;
    src: url("${interMedium}") format("woff2");
  }
  
  @font-face {
    font-family: 'Inter';
    font-style:  normal;
    font-weight: 700;
    src: url("${interBold}") format("woff2");
  }
`;