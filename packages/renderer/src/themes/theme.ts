import * as baseStyled from 'styled-components';
import {ThemedStyledComponentsModule} from 'styled-components';
import {ThemeInterface} from '@denokon/comp-lib-3';

export interface Theme extends ThemeInterface
{    
    // App
    appBorderColor: string;
    primaryColor: string;
    textColorOnPrimary: string;
    
    // General
    headerBackgroundColor: string;
    
    // Sidebar
    sidebarHeaderBackgroundColor: string;
    sidebarHoverBackgroundColor: string;
    activeSidebarLinkColor: string;
    sidebarBackgroundColor: string;
    
    // Page
    pageTitleBorderColor: string;
}

// Based on: https://gist.github.com/chrislopresto/490ef5692621177ac71c424543702bbb
const { default: styled, ThemeProvider, withTheme, createGlobalStyle, css, ThemeContext } = baseStyled as ThemedStyledComponentsModule<Theme>;

export { styled, ThemeProvider, withTheme, createGlobalStyle, css, ThemeContext };