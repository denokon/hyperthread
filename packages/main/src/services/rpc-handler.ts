import {RpcHandler} from '@epicentric/rxpc';

export const rpcHandler = new RpcHandler();
export const rpcHook = rpcHandler.hooks.hook;