import {createToken, Lexer} from 'chevrotain';

export const Whitespace = createToken({
    name: 'Whitespace',
    pattern: /\s+/,
    group: Lexer.SKIPPED
});