export const snapToGridSize = 25;

export function snapToGrid(x: number, y: number): [number, number]
{
    const snappedX = Math.round(x / snapToGridSize) * snapToGridSize;
    const snappedY = Math.round(y / snapToGridSize) * snapToGridSize;
    return [snappedX, snappedY]
}