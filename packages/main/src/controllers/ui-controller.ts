import {UiControllerInterface} from '@hyperthread/common/rpc-interfaces/ui-controller-interface';
import {rpcCallable, RpcRequest} from '@epicentric/rxpc';
import {app, dialog} from 'electron';
import {ServerInterface} from '@hyperthread/common/rpc-interfaces/server-interface';
import {getWindow} from '../utils/get-window';

export class UiController implements ServerInterface<UiControllerInterface>
{
    @rpcCallable()
    public async browseDirectory(request: RpcRequest, title: string, startingPath?: string): Promise<string | null>
    {
        const browserWindow = getWindow(request);

        const { filePaths = null } = await dialog.showOpenDialog(browserWindow, {
            title: title,
            defaultPath: startingPath,
            properties: ['openDirectory']
        });
        
        return filePaths && filePaths[0];
    }

    @rpcCallable()
    public getDefaultCreatePath(request: RpcRequest): string
    {
        return app.getPath('documents');
    }

    @rpcCallable()
    public minimize(request: RpcRequest): void
    {
        const browserWindow = getWindow(request);
        
        browserWindow.minimize();
    }

    @rpcCallable()
    public close(request: RpcRequest): void
    {
        const browserWindow = getWindow(request);

        browserWindow.close();        
    }
}

export const uiController = new UiController;