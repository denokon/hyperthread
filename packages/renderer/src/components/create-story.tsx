import {ErrorMessage, FastField, Formik, FormikActions} from 'formik';
import React, {useCallback, useRef} from 'react';
import {styled} from '../themes/theme';
import {storyInterface, uiInterface} from '../services/rpc-client';
import {history} from '../history';
import {useObservable} from 'rxjs-hooks';
import {from} from 'rxjs';

export interface CreateStoryParams
{
    name: string;
    path: string;
    createDirectory: boolean;
}

export function CreateStory()
{
    const defaultPath = useObservable(() => from(uiInterface.getDefaultCreatePath()), null);
    
    const formikRef = useRef<Formik<CreateStoryParams>>(null);
    
    const onSubmit = useCallback(async (values: CreateStoryParams, {setSubmitting}: FormikActions<CreateStoryParams>): Promise<void> => {
        setSubmitting(true);
        try
        {
            const story = await storyInterface.create(values);
            
            history.push(`/stories/${story.id}`);
        }
        finally
        {
            setSubmitting(false);
        }
    }, []);
    
    const onBrowse = useCallback(async(): Promise<void> => {
        const formik = formikRef.current;
        
        if (!formik)
            return;
        
        let path = formik.getFormikContext().values.path;
        
        const filePath = await uiInterface.browseDirectory('New story project\'s path', path);

        if (!filePath)
            return;

        formik.setFieldValue('path', filePath);
    }, []);
    
    if (!defaultPath)
        return null;
    
    return <Formik
        ref={formikRef}
        initialValues={{
            name: 'New story',
            path: defaultPath,
            createDirectory: true
        }}
        onSubmit={onSubmit}
    >
        {({handleReset, handleSubmit, isSubmitting, values}) => <StyledCreateStory onReset={handleReset} onSubmit={handleSubmit}>
            <ModalHeader>
                Start New Story
            </ModalHeader>
            <ModalBody>
                <FieldName>Story name:</FieldName>
                <div>
                    <TextField name="name" />
                    <ErrorMessage name="name" />
                </div>
                <div />

                <FieldName>Path:</FieldName>
                <div>
                    <TextField name="path" />
                    <ErrorMessage name="path" />
                </div>
                <div>
                    <button type="button" onClick={onBrowse}>Browse...</button>
                </div>

                <CreateDirectoryWrapper>
                    <label>
                        <CheckboxField name="createDirectory" checked={values.createDirectory} />
                        Create directory for story under path
                    </label>
                </CreateDirectoryWrapper>
            </ModalBody>
            <ModalFooter>
                <button type="submit" disabled={isSubmitting}>Create</button>
            </ModalFooter>
        </StyledCreateStory>}
    </Formik>;
}

const ModalHeader = styled.div``;
const ModalBody = styled.div``;
const ModalFooter = styled.div``;

const TextField = styled(FastField).attrs({ type: 'text' })`
    appearance: none;
    align-items: center;
    border: 1px solid transparent;
    border-radius: 4px;
    box-shadow: none;
    display: inline-flex;
    height: 2.25em;
    justify-content: flex-start;
    line-height: 1.5;
    padding-bottom: calc(.375em - 1px);
    padding-left: calc(.625em - 1px);
    padding-right: calc(.625em - 1px);
    padding-top: calc(.375em - 1px);
    position: relative;
    vertical-align: top;

    outline: 0;
    background-color: #fff;
    border-color: #dbdbdb;
    color: #363636;
    box-shadow: inset 0 1px 2px rgba(10,10,10,.1);
    max-width: 100%;
    width: 100%;
    
    :focus {
        border-color: #3273dc;
        box-shadow: 0 0 0 0.125em rgba(50,115,220,.25);
    }
`;

const CheckboxField = styled(FastField).attrs({ type: 'checkbox' })`

`;

const StyledCreateStory = styled.form`
  font-size: 1.4rem;
  text-align: left;
`;

const CreateStoryFields = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  column-gap: .7rem;
  row-gap: 1.2rem;
  align-items: center;
  margin-bottom: 1.2rem;
  
  ${TextField} {
    min-width: 25rem;
  }
`;

const CreateDirectoryWrapper = styled.div`
  grid-column: span 3;
  
  label {
    margin: 0;
    display: flex;
    align-items: center;
  }
  
  input {
    margin-right: .5rem;
  }
`;

const FormSubmitWrapper = styled.div`
  text-align: right;
`;

const FieldName = styled.div`
  text-align: right;
`;