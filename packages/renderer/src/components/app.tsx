import {BaseComponent} from '@epicentric/utilbelt-react';
import React from 'react';
import {Redirect, Route, Router, Switch} from 'react-router';
import {history} from '../history';
import {StartPage} from './pages/start-page/start-page';
import {defaultTheme} from '../themes/default-theme';
import {styled, ThemeProvider} from '../themes/theme';
import {GlobalStyle} from './global-style';
import {PreviewPage} from './pages/preview-page';
import {StoryMapPage} from './pages/story-map-page/story-map-page';
import HTML5Backend from 'react-dnd-html5-backend';
import {BaseModalBackground, ModalProvider} from 'styled-react-modal';
import {zIndexModalBackground} from '../themes/z-indexes';
import {DndProvider} from 'react-dnd';
import {GlobalStyle as CompGlobalStyle} from '@denokon/comp-lib-3';

const StyledApp = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  border: 1px solid ${({theme}) => theme.appBorderColor};
`;

const FadingBackground = styled(BaseModalBackground)<{isOpenStyle: boolean}>`
  z-index: ${zIndexModalBackground}; 
  opacity: ${props => props.isOpenStyle ? 1 : 0};
  transition: opacity ease-in-out .25s;
`;

export class App extends BaseComponent
{
    public render(): React.ReactNode
    {
        return <ThemeProvider theme={defaultTheme}>
            <DndProvider backend={HTML5Backend}>
                <ModalProvider backgroundComponent={FadingBackground}>
                    <>
                        <CompGlobalStyle />
                        <GlobalStyle />
                        <Router history={history}>
                            <StyledApp>
                                <Switch>
                                    <Route path="/start" component={StartPage} />
                                    <Route path="/stories/:path/passages/:passage" component={StoryMapPage} />
                                    <Route path="/stories/:path" component={StoryMapPage} />
                                    {/*<Route path="/stories/:path" component={EditorPage} />*/}
                                    <Route path="/preview/:path" component={PreviewPage} />
                                    <Redirect to="/start" />
                                </Switch>
                            </StyledApp>
                        </Router>
                    </>
                </ModalProvider>
            </DndProvider>
        </ThemeProvider>;
    }
}