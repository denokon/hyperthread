import {PlaySession} from '@hyperthread/engine/story/play-session';
import localforage from "localforage";
import {StateInterface} from '@hyperthread/engine/story/state';

export interface StoredHistory
{
    buffer: StateInterface[];
    currentIndex: number;
}

const saveKey = 'story-state';
const maxHistorySize = 20;

export async function persistenceLocalforagePlugin(session: PlaySession)
{
    const store = localforage.createInstance({
        name: 'Hyperthread',
        storeName: `story-${session.story.id}`
    });
    
    session.on('start', async () => {
        const storedHistory = await store.getItem<StoredHistory>(saveKey);

        if (storedHistory)
        {
            await session.emit('load', storedHistory);
            
            session.history.setBuffer(storedHistory.buffer, storedHistory.currentIndex, maxHistorySize);
        }
    });
    
    session.on('historyPostPush', async () => {
        const storedHistory = {
            buffer: session.history.buffer.toArray() as StateInterface[],
            currentIndex: session.history.currentIndex
        };
        
        await session.emit('save', storedHistory);
        
        await store.setItem<StoredHistory>(saveKey, storedHistory);
    });
}