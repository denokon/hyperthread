import * as React from 'react';
import {ReactNode} from 'react';
import {BaseComponent, tracked} from '@epicentric/utilbelt-react';
import {styled, ThemeProvider} from '@hyperthread/engine/themes/theme';
import {defaultTheme} from '@hyperthread/engine/themes/default-theme';
import {GlobalStyle} from '@hyperthread/engine/templates/global-style';
import {getPassage, getStartPassage, PlaySession} from '@hyperthread/engine/story/play-session';
import {animated, Transition} from 'react-spring/renderprops';
import {renderPassage} from '@hyperthread/engine/story/passage-renderer';
import {switchMap, tap} from 'rxjs/operators';
import {combineLatest, fromEvent, merge} from 'rxjs';

const Theatre = styled.div`
  display: flex;
  justify-content: center;
  padding: 4.5rem 1.5rem 0;
  
  > * + * {
    margin-left: 1.5rem;
  }
`;

const Passage = styled(animated.div)`
  padding: 2rem;
  background: #282828;
  border: 1px solid #3e3e3e;
  box-shadow: inset 0 0 1rem 0.8rem rgba(0, 0, 0, 0.05);
`;

const SidebarPassage = styled(Passage)`
  width: 20rem;
`;

const CurrentPassage = styled(Passage)`
  position: absolute;
  width: 100%;
`;

const CurrentPassageWrapper = styled.div`
  position: relative;
  width: 60rem;
`;

export interface AppProps
{
    session: PlaySession;
}

export class DefaultTemplate extends BaseComponent<AppProps>
{
    @tracked
    private currentRender: { mainRender: ReactNode, sidebarRender: ReactNode, index: number } = { mainRender: null, sidebarRender: null, index: -1 };
    
    private mainRenders: Map<number, ReactNode> = new Map;
    
    public constructor(props: AppProps, context: any)
    {
        super(props, context);
        
        const {session} = props;
        
        this.addObservable('currentRender', merge(
            fromEvent(session, 'postStart'),
            fromEvent(session, 'historyPostPush')
        ).pipe(
            switchMap(async () => {
                const story = session.story;
                const passageName = session.history.current.passageName;

                const mainPassage = getPassage(story, passageName) || getStartPassage(story);
                const sidebarPassage = getPassage(story, 'Sidebar');

                let mainRender: ReactNode = null;
                let sidebarRender: ReactNode = null;
                
                await this.props.session.render(context => {
                    mainRender = mainPassage
                        ? renderPassage(mainPassage.node, context)
                        : `Could not find passage named '${passageName}' and found no start passages. Something is very wrong.`;

                    context.clearLocal(); // Clear locals between passages
                    
                    sidebarRender = sidebarPassage
                        ? renderPassage(sidebarPassage.node, context)
                        : null;

                    context.clearLocal(); // Release locals after render
                });
                
                return {
                    mainRender,
                    sidebarRender,
                    index: this.props.session.history.currentIndex
                }
            }),
            tap(data => this.mainRenders.set(data.index, data.mainRender))
        ));
    }

    public render(): React.ReactNode
    {
        const { sidebarRender, index } = this.currentRender;
        
        return <ThemeProvider theme={defaultTheme}>
            <>
                <GlobalStyle />
                <Theatre>
                    {sidebarRender && <SidebarPassage>{sidebarRender}</SidebarPassage>}
                    <CurrentPassageWrapper>
                        <Transition
                            native
                            reset
                            unique
                            items={index}
                            from={{ opacity: 0, transform: 'translate3d(0%,20%,0)' }}
                            enter={{ opacity: 1, transform: 'translate3d(0%,0,0)' }}
                            leave={{ opacity: 0 }}
                            onDestroyed={destroyedIndex => this.mainRenders.delete(destroyedIndex)}
                        >
                            {currentIndex => style => <CurrentPassage style={style}>{this.mainRenders.get(currentIndex)}</CurrentPassage>}
                        </Transition>
                    </CurrentPassageWrapper>
                </Theatre>
            </>
        </ThemeProvider>;
    }
}