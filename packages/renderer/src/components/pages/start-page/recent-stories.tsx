import {stringEncode} from '@hyperthread/common/string-encode';
import React, {ReactNode} from 'react';
import {StoryInterface} from '@hyperthread/common/models/story-interface';
import {styled} from '../../../themes/theme';
import {Link} from 'react-router-dom';
import ReactWordcloud from 'react-wordcloud';
import XRegExp from 'xregexp';
import {Options} from 'react-wordcloud/dist/types';
import {clamp} from 'lodash-es';

interface RecentStoriesProps
{
    stories: StoryInterface[];
}

const wordcloudOptions: Partial<Options> = {
    fontFamily: 'Inter',
    deterministic: true,
    enableTooltip: false,
    transitionDuration: 0,
    fontSizes: [14, 32],
    rotations: 3,
    rotationAngles: [0, 75]
};

const wordcloudSize: [number, number] = [200, 200];

export function RecentStories({stories}: RecentStoriesProps)
{
    return <StyledRecentStories>
        {stories.map(story => <RecentStory key={story.path} to={`stories/${stringEncode(story.path)}`}>
            {/*<StoryPassageCount>{story.passages.length}</StoryPassageCount>*/}
            <WordcloudWrapper>
                <SimpleWordcloud words={getStoryWords(story)} />
                {/*<ReactWordcloud*/}
                {/*    minSize={wordcloudSize}*/}
                {/*    size={wordcloudSize}*/}
                {/*    options={wordcloudOptions}*/}
                {/*    words={getStoryWords(story)}*/}
                {/*/>*/}
            </WordcloudWrapper>
            <StoryInfo>
                <StoryTitle>{story.name}</StoryTitle>
                {/*<StorySubInfo>{story.path}</StorySubInfo>*/}
            </StoryInfo>
        </RecentStory>)}
    </StyledRecentStories>;
}

const wordRegex = XRegExp(`[\\p{L}']{3,}`, 'g');

function getStoryWords(story: StoryInterface)
{
    const words = new Map<string, number>();
    
    for (const passage of story.passages)
    {
        for (const [match] of passage.content.matchAll(wordRegex))
        {
            const normalizedWord = match.replace(/^'|'$/g, '').toLocaleLowerCase();
            const count = words.get(normalizedWord) || 0;
            words.set(normalizedWord, count + 1);
        }
    }
    
    const result = Array.from(words).map(([word, count]) => ({ text: word, value: count }));
    result.sort((a, b) => b.value - a.value);
    return result;
}

function SimpleWordcloud({words}: { words: {text: string, value: number}[] }): any
{
    let result: ReactNode[] = [];

    if (0 < words.length)
    {
        let index = 0;
        const maxCount = words[0].value;
        const minCount = words[Math.min(30, words.length - 1)].value;

        const countDiff = Math.max(1, maxCount - minCount);
        for (const {text, value} of words)
        {
            const fontSize = 1 + clamp(value - minCount, 0, countDiff) / countDiff * 1;
            result.push(<span key={text} style={{fontSize: `${fontSize}em`}}>{text} </span>);

            if (30 <= ++index)
                break;
        }
    }
    
    return <div style={{textAlign: 'justify', textAlignLast: 'justify'}}>{result}</div>;
}

function lerp(v0: number, v1: number, t: number) {
    return v0*(1-t)+v1*t
}

const StyledRecentStories = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

const StoryPassageCount = styled.div`
    font-size: 3.8rem;
    line-height: .8;
    padding-right: 1.5rem;
    position: relative;
    top: -0.05em;
    opacity: .7;
    transition: opacity .2s ease-in-out;
`;

const StoryInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

const StoryTitle = styled.div`
  font-size: 1.8rem;
  font-weight: bold;
  line-height: 1;
  padding-bottom: .4rem;
`;

const StorySubInfo = styled.div`
  font-size: 1.1rem;
  line-height: 1.2;
  opacity: .7;
  transition: opacity .2s ease-in-out;
`;

const WordcloudWrapper = styled.div`
  width: 20rem;
  height: 18rem;
  font-size: 1rem;
  line-height: 2em;
  padding: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #464646; // TODO: theme
  margin-bottom: 1rem;
`;

const RecentStory = styled(Link)`
  margin: .5rem;

  padding: .6rem .6rem;
  transition: background-color .2s ease-in-out;
  cursor: pointer;
  color: inherit;
  
  :hover {
    background-color: #444;
    text-decoration: none;
    color: inherit;
    
    ${StoryPassageCount}, ${StorySubInfo} {
      opacity: 1;
    }
  }
` as unknown as typeof Link;