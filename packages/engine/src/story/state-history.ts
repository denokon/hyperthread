import {createEmptyState, StateInterface} from '@hyperthread/engine/story/state';
import {CircularBuffer} from 'mnemonist';
import {ReplaySubject} from 'rxjs';

export class StateHistory
{
    public get canGoBack(): boolean
    {
        return 0 < this.currentIndex;
    }
    
    public get canGoForward(): boolean
    {
        return this.currentIndex < this.buffer.size - 1;
    }
    
    public get current(): StateInterface
    {
        return this.buffer.get(this.currentIndex) || createEmptyState();
    }
    
    public current$: ReplaySubject<StateInterface>;
    
    public buffer!: CircularBuffer<StateInterface>;
    public currentIndex: number = 0;

    public constructor(bufferSize: number)
    {
        this.current$ = new ReplaySubject(1);
        this.setBuffer([createEmptyState()], 0, bufferSize);
    }
    
    public setBuffer(buffer: StateInterface[], currentIndex: number, bufferSize: number)
    {
        // TODO: validate currentIndex
        
        this.buffer = CircularBuffer.from(buffer, Array, bufferSize);
        this.currentIndex = currentIndex;
        this.current$.next(this.current);
    }
    
    public push(state: StateInterface): void
    {
        while (this.currentIndex < this.buffer.size - 1)
        {
            this.buffer.pop();
        }
        
        this.buffer.push(state);
        this.forward();
    }
    
    public back(): void
    {
        if (!this.canGoBack)
            return;
        
        this.currentIndex = Math.max(0, this.currentIndex - 1);
        this.current$.next(this.current);
    }
    
    public forward(): void
    {
        if (!this.canGoForward)
            return;
        
        this.currentIndex = Math.min(this.buffer.size - 1, this.currentIndex + 1);
        this.current$.next(this.current);
    }
    
    public clear(): void
    {
        this.buffer.clear();
        this.buffer.push(createEmptyState());
        this.currentIndex = 0;
        this.current$.next(this.current);
    }
}