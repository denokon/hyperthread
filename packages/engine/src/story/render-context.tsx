import {createStateProxy, StateInterface} from '@hyperthread/engine/story/state';
import {PlaySession} from '@hyperthread/engine/story/play-session';
import {StateHistory} from '@hyperthread/engine/story/state-history';

export class RenderContext
{
    public get stateProxy(): StateInterface
    {
        return this._stateProxy || (this._stateProxy = createStateProxy(this));
    }
    
    public get history(): StateHistory
    {
        return this.session.history;
    }

    public local: StateInterface = {};

    // Initialize to undefined for performance
    private _stateProxy?: StateInterface = undefined;

    public constructor(public readonly session: PlaySession, public global: StateInterface)
    {
    }

    public async setState(recipe: (draft: StateInterface) => void)
    {
        await this.session.renderState(this, recipe);        
        this.session.history.push(this.global);
    }
    
    public clearLocal(): void
    {
        this.local = {};
    }
}