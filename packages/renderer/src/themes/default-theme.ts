import {Theme} from './theme';
import {defaultTheme as libTheme} from '@denokon/comp-lib-3';

export const defaultTheme: Theme = {
    ...libTheme,
    
    // App
    bodyBg: '#262626',
    bodyColor: '#e2e2e2',
    fontFamilyBase: 'Inter',
    appBorderColor: '#424242',
    primaryColor: '#deae2c',
    textColorOnPrimary: '#1c1c1c',
    
    // General
    headerBackgroundColor: '#CCC',
    
    // Sidebar
    sidebarHeaderBackgroundColor: '#BBB',
    sidebarHoverBackgroundColor: '#CCC',
    activeSidebarLinkColor: 'red',
    sidebarBackgroundColor: '#F4F5F6',
    
    // Page
    pageTitleBorderColor: 'black',
};