import {darken, lighten, transitions} from 'polished';
import React from 'react';
import {css, styled, Theme} from '../themes/theme';
import {StyledComponent} from 'styled-components';

const buttonBackground = {
    default: 'rgb(221, 221, 221)',
    success: '#20bc56'
};

const buttonColor = {
    default: 'black',
    success: 'white'
};

export const Button = styled.button.attrs({ type: 'button' })<{ backgroundColor?: string, color?: string }>`
  background-color: ${props => props.backgroundColor || props.theme.primaryColor};
  color: ${props => props.color || props.theme.textColorOnPrimary};
  border: none;
  box-shadow: 0 1px 1px rgba(0,0,0,.25);
  padding: .5rem 1rem;
  cursor: pointer;
  border-radius: .25rem;
  outline: none;
  font-size: 1.4rem;
  transition: background-color .2s ease-in-out;
  
  :hover {
    background-color: ${props => lighten(.1, props.backgroundColor || props.theme.primaryColor)};
  }
`;

export const PlainButton = styled.button.attrs({ type: 'button' })<{ active?: boolean }>`
  background: none;
  border: none;
  padding: 0;
  color: ${props => props.active ? props.theme.primaryColor : 'inherit'};
  cursor: pointer;
  transition: .2s color ease-in-out, .2s background-color ease-in-out;
  
  &:focus {
    outline: 0;
  }
`;

interface RoundButtonProps
{
    buttonStyle: 'default'|'success';
    outline: boolean;
}

function getRoundButtonBackground(props: RoundButtonProps)
{
    return !props.outline ? buttonBackground[props.buttonStyle] : 'none';
}

function getRoundButtonBorder(props: RoundButtonProps)
{
    return props.outline ? `1px solid ${buttonBackground[props.buttonStyle]}` : 'none';
}

export const RoundButton = styled.button.attrs((props: RoundButtonProps) => ({
    type: 'button',
    buttonStyle: props.buttonStyle || 'default',
    outline: undefined === props.outline ? false : props.outline
}))<RoundButtonProps>`
  display: inline-block;
  padding: 0;
  border: ${getRoundButtonBorder};
  background: ${getRoundButtonBackground};
  color: ${(props: RoundButtonProps) => !props.outline ? buttonColor[props.buttonStyle] : 'inherit'};
  border-radius: 50%;
  width: 2.6em;
  height: 2.6em;
  line-height: 2.6em;
  text-align: center;
  cursor: pointer;
  box-shadow: inset 0 0 .4rem black;
  ${transitions(['background-color', 'border-color', 'box-shadow'], 'ease-in-out .2s')};
  
  &:hover {
    border-color: ${(props: RoundButtonProps) => darken(.2, buttonBackground[props.buttonStyle])};
    background: ${(props: RoundButtonProps) => darken(.08, buttonBackground[props.buttonStyle])};
  }
` as unknown as StyledComponent<'button', Theme, Partial<RoundButtonProps>>;