import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// @ts-ignore
import CircularDependencyPlugin from 'circular-dependency-plugin';

import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import {TsconfigPathsPlugin} from 'tsconfig-paths-webpack-plugin';
import {Configuration} from 'webpack';

// https://github.com/dividab/tsconfig-paths-webpack-plugin/issues/31#issuecomment-447655199
process.env['TS_NODE_PROJECT'] = '';

const distDir = path.join(__dirname, 'dist');
const distFile = 'story.js';

const config: Configuration = {
    target: 'web',

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        plugins: [
            new TsconfigPathsPlugin()
        ],
        symlinks: false,
        modules: [
            path.resolve(__dirname, '../node_modules'),
            path.resolve(__dirname, '../../../node_modules'),
            'node_modules'
        ]
    },
    
    devtool: 'source-map',
    mode: 'development',
    output: {
        path: distDir,
        filename: distFile,
        publicPath: '/'
    },
    devServer: {
        stats: 'minimal',
        historyApiFallback: {
            disableDotRule: true
        },
        // hot: true,
        hot: false,
        overlay: {
            warnings: true,
            errors: true
        }
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            poolRespawn: false
                        }
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            envName: 'development',
                            configFile: path.resolve(__dirname, '../src/webpack-build/babel.config.js')
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: true,
                            configFile: path.resolve(__dirname, '../src/webpack-build/tsconfig.json')
                        }
                    }
                ]
            },
            {
                test: /\.jsx$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            poolRespawn: false
                        }
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            envName: 'development',
                            configFile: path.join(__dirname, '../src/webpack-build/babel.config.js')
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|svg|jpg|gif|woff2?|eot|ttf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        // new HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'Hyperthread'
        }),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
};

export default config;