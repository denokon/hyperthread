export const enum AstNodeType
{
    Passage,
    PlainText,
    HardLineBreak,
    ParagraphSeparator,
    Content,
    Expression,
    Code,
    Unknown
}

export interface AstNode
{
    type: AstNodeType;
}

export interface HardLineBreakAstNode
{
    type: AstNodeType.HardLineBreak;
}

export interface ParagraphSeparatorAstNode
{
    type: AstNodeType.ParagraphSeparator;
}

export interface ContentAstNode
{
    type: AstNodeType.Content,
    children: AstNodeUnion[]
}

export interface PassageAstNode
{
    type: AstNodeType.Passage;
    content: ContentAstNode;
}

export interface PlainTextAstNode
{
    type: AstNodeType.PlainText;
    content: string;
}

export interface ExpressionAstNode
{
    type: AstNodeType.Expression;
    name: string;
    parameters: string[];
    content?: ContentAstNode;
}

export interface CodeAstNode
{
    type: AstNodeType.Code,
    isBlock: boolean;
    content: string;
}

export interface UnknownAstNode
{
    type: AstNodeType.Unknown;
}

export type AstNodeUnion = ContentAstNode | PassageAstNode | PlainTextAstNode | ParagraphSeparatorAstNode | HardLineBreakAstNode | ExpressionAstNode | CodeAstNode | UnknownAstNode;