import {
    CallRpcMessage,
    DebugEventType,
    ErrorRpcMessage,
    MsgpackRpcEncoder,
    NextRpcMessage,
    RpcClient,
    RpcMessage,
    RpcMessageType
} from '@epicentric/rxpc';
import {rpcHandler} from './rpc-handler';
import {StoreInterface} from '@hyperthread/common/rpc-interfaces/store-interface';
import {ElectronRendererTransport} from '@epicentric/rxpc-transport-electron';
import {BrowserIdGenerator, fromBinaryId} from '@epicentric/binary-id';
import {LoggerOutput} from '@epicentric/logger';
import {customFormatterSymbol} from '@epicentric/logger-browser-output';
import {logger} from './logger';
import {LRUMap} from 'mnemonist';
import {
    StoryControllerInterface,
    storyControllerPromiseFunctions
} from '@hyperthread/common/rpc-interfaces/story-controller-interface';
import {
    UiControllerInterface,
    uiControllerPromiseFunctions
} from '@hyperthread/common/rpc-interfaces/ui-controller-interface';

const transportId = new Uint8Array(16);
crypto.getRandomValues(transportId);

const idGenerator = new BrowserIdGenerator;

export const rpcClient = new RpcClient({
    encoder: new MsgpackRpcEncoder(),
    idGenerator: idGenerator,
    transport: new ElectronRendererTransport({
        id: idGenerator.create()
    }),
    pingEnabled: false
});

rpcClient.message.subscribe(message => rpcHandler.handle(rpcClient, message, {}));
rpcClient.connect();

export const storeInterface = rpcHandler.createProxyClass<StoreInterface>(rpcClient, 'store', () => ({}));
export const storyInterface = rpcHandler.createProxyClass<StoryControllerInterface>(rpcClient, 'story', () => ({}), storyControllerPromiseFunctions);
export const uiInterface = rpcHandler.createProxyClass<UiControllerInterface>(rpcClient, 'ui', () => ({}), uiControllerPromiseFunctions);

function logRpcMessage(message: RpcMessage, incoming: boolean): void
{
    if (message.type === RpcMessageType.Receipt)
        return;

    const firstIcon = incoming ? '🌠' : '🚀';
    const idString = fromBinaryId(message.originalId || message.id);

    let secondIcon: string;
    switch (message.type)
    {
        case RpcMessageType.Call:
            secondIcon = '🚚';
            break;
        case RpcMessageType.Next:
            secondIcon = '📦';
            break;
        case RpcMessageType.Complete:
            secondIcon = '🏁';
            break;
        case RpcMessageType.Unsubscribe:
            secondIcon = '✋';
            break;
        case RpcMessageType.Error:
            secondIcon = '😈';
            break;
        // case RpcMessageType.Receipt:
        //     secondIcon = '🧾';
        //     break;
        case RpcMessageType.Ping:
            secondIcon = '🏓';
            break;
        default:
            secondIcon = '❓';
            break;
    }

    let callMessage = messageCache.get(fromBinaryId(message.originalId || message.id));

    if (callMessage)
    {
        logger.debug({
            [customFormatterSymbol]: (chunk: LoggerOutput, timestamp: string) => {
                const cleanArgs: any[] = callMessage!.message.args.slice();

                for (let i = cleanArgs.length - 1; 0 <= i; i--)
                {
                    if (cleanArgs[i] === undefined)
                        cleanArgs.pop();
                    else
                        break;
                }

                for (let i = 1; i < cleanArgs.length; i+=2)
                {
                    cleanArgs.splice(i, 0, ',');
                }

                const consoleArgs: any[] = [
                    timestamp,
                    secondIcon,
                    callMessage!.counter.toString(16).padStart(3, '0'),
                    // fromBinaryId(originalMessage.id),
                    // originalMessage
                ];

                if (message.type === RpcMessageType.Call)
                    consoleArgs.push(callMessage!.message.name + '(', ...cleanArgs, ')');
                else if (message.type === RpcMessageType.Next)
                    consoleArgs.push((message as NextRpcMessage).value);
                else if (message.type === RpcMessageType.Error)
                    consoleArgs.push((message as ErrorRpcMessage).error);

                return consoleArgs;
            }
        });
    }
    else
    {
        logger.debug(message, `${firstIcon} ${secondIcon} ${idString}`);
    }
}

let messageCounter = 0;

const messageCache = new LRUMap<string, { message: CallRpcMessage, counter: number }>(20);

rpcClient.debugEvent.subscribe(event => {
    switch (event.type)
    {
        case DebugEventType.StartConnecting:
            logger.log('Connecting...');
            break;
        case DebugEventType.FinishConnecting:
            logger.log('Finished connecting.');
            break;
        case DebugEventType.Error:
            logger.error({ error: event.error }, 'RPC error');
            break;
        case DebugEventType.IncomingMessage:
            const incomingType = event.message.type;

            if (incomingType === RpcMessageType.Call)
                messageCache.set(fromBinaryId(event.message.id), { message: event.message as CallRpcMessage, counter: messageCounter++ });

            if (incomingType === RpcMessageType.Call
                || incomingType === RpcMessageType.Next
                || incomingType === RpcMessageType.Complete)
            {
            }

            logRpcMessage(event.message, true);
            break;
        case DebugEventType.OutgoingMessage:
            const outgoingType = event.message.type;

            if (outgoingType === RpcMessageType.Call)
                messageCache.set(fromBinaryId(event.message.id), { message: event.message as CallRpcMessage, counter: messageCounter++ });

            if (outgoingType === RpcMessageType.Call
                || outgoingType === RpcMessageType.Next
                || outgoingType === RpcMessageType.Complete)
            {
            }

            logRpcMessage(event.message, false);
            break;
    }
});