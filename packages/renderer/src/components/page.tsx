import {styled} from '../themes/theme';
import {TitleBarHeight} from './title-bar';

export const Page = styled.div`
  flex-grow: 1;
`;

export const Main = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: ${TitleBarHeight} 0 0;
  height: 100%;
`;

export const Sidebar = styled.aside`
  border-left: 1px solid #464646;
  background-color: #2f2f2f;
  padding: calc(${TitleBarHeight} + 1rem) 1.2rem 0;
  height: 100%;
`;