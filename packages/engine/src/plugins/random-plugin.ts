import {PlaySession} from '@hyperthread/engine/story/play-session';
import {RenderContext} from '@hyperthread/engine/story/render-context';
import seedrandom, {State} from 'seedrandom';
import {Class} from 'utility-types';
import {sortedIndex, sum} from 'lodash-es';
import {StoredHistory} from '@hyperthread/engine/plugins/persistence-localforage-plugin';
import produce from 'immer';

export function randomPlugin(session: PlaySession)
{
    session.on('onRender', (context: RenderContext) => {
        if (!context.global.random)
            context.global.random = new Random();
        else
            context.global.random = new Random(context.global.random.getState());
    });
    
    session.on('load', (storedHistory: StoredHistory) => {
        for (const entry of storedHistory.buffer)
        {
            const randomState: State|undefined = entry.__randomState;
            
            if (randomState)
                entry.random = new Random(randomState);
            
            delete entry.__randomState;
        }
    });

    session.on('save', (storedHistory: StoredHistory) => {
        storedHistory.buffer = produce(storedHistory.buffer, draft => {
            for (const entry of draft)
            {
                const random: Random|undefined = entry.random;

                if (random)
                {
                    entry.__randomState = random.getState();
                    delete entry.random;
                }
            }
        });
    });
}

export class Random
{
    private prng: seedrandom.prng;

    public constructor(state?: State)
    {
        // PRNG for seeded random number generation
        this.prng = new (seedrandom as unknown as Class<seedrandom.prng>)(null, {
            state: state || true,
            global: false
        } as seedrandom.seedRandomOptions);
    }

    public random(): number
    {
        return this.prng.double();
    }

    public weighted<T>(values: T[], weights: number[]): T
    {
        const total = sum(weights);

        let carry = 0;
        const normalizedWeights = weights.map(value => carry = carry + value / total);

        const value = this.random();

        return values[sortedIndex(normalizedWeights, value)];
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    public float(min: number, max: number): number
    {
        return Math.random() * (max - min) + min;
    }

    public integer(min: number, max: number): number
    {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(this.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
    }

    public shuffle<T>(items: T[]): T[]
    {
        const oldItems: T[] = items.slice();
        const newItems: T[] = [];

        while (oldItems.length)
        {
            newItems.push(...oldItems.splice(this.integer(0, oldItems.length-1), 1));
        }

        return newItems;
    }

    public pickSet<T>(items: T[], quantity: number): T[]
    {
        if (1 === quantity)
            return [ this.pick(items) ];

        return this.shuffle(items).slice(0, quantity);
    }

    public pick<T>(items: T[]): T
    {
        return items[this.integer(0, items.length - 1)];
    }
    
    public getState()
    {
        return this.prng.state();
    }
}