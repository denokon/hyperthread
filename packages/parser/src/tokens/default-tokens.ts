import {createToken} from 'chevrotain';
import {blockCodeMode, expressionMode, inlineCodeMode} from './token-modes';

export const LCurly = createToken({
    name: 'LCurly',
    pattern: /{/,
    push_mode: expressionMode
});

export const LCurlyBlockStart = createToken({
    name: 'LCurlyBlockStart',
    pattern: /{#/,
    push_mode: expressionMode
});

export const LCurlyBlockEnd = createToken({
    name: 'LCurlyBlockEnd',
    pattern: /{\//,
    push_mode: expressionMode
});

export const PlainText = createToken({
    name: 'PlainText',
    // - Match a string that does not contain the following characters: {, `
    // - Stop matching if either of the following succeed the match:
    //      1. Two or more spaces, followed by a line break
    //      2. A backslash, followed by a line break
    pattern: /(?:(?! {2,}\n|\\\n)[^\n{`])+/
});

export const HardLineBreak = createToken({
    name: 'HardLineBreak',
    pattern: / {2,}\n|\\\n/
});

export const ParagraphSeparator = createToken({
    name: 'ParagraphSeparator',
    pattern: /\n(?:[ \t]*\n)+/
});

export const SoftLineBreak = createToken({
    name: 'SoftLineBreak',
    pattern: /\n/,
    longer_alt: ParagraphSeparator
});

export const TripleBacktick = createToken({
    name: 'TripleBacktick',
    pattern: /```/,
    push_mode: blockCodeMode,
});

export const Backtick = createToken({
    name: 'Backtick',
    pattern: /`/,
    push_mode: inlineCodeMode,
    longer_alt: TripleBacktick,
});

export const EscapedCharacter = createToken({
    name: 'EscapedCharacter',
    pattern: /\\\S/
});

export const defaultTokens = [
    // Whitespace, // Whitespace in front for performance reasons
    PlainText,
    HardLineBreak,
    ParagraphSeparator,
    SoftLineBreak,
    EscapedCharacter,
    LCurlyBlockStart,
    LCurlyBlockEnd,
    LCurly,
    TripleBacktick,
    Backtick,
];

