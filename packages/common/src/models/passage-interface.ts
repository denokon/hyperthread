import nanoid from 'nanoid';

export interface PassageInterface
{
    id: string;
    filename?: string;
    name: string;
    content: string;
    gridPosition: [number, number];
    isStart?: boolean;
}

export function generatePassageId()
{
    return nanoid();
}

export function createEmptyPassage(): PassageInterface
{
    return {
        id: '',
        name: 'Default passage name',
        content: '',
        gridPosition: [50, 50]
    };
}