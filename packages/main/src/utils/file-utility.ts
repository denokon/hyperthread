import baseSanitizeFilename from 'sanitize-filename';

export function sanitizeFilename(input: string): string
{
    return baseSanitizeFilename(input.replace(/\./, ''));
}