import React, {ReactNode} from 'react';
import {
    AstNodeType,
    AstNodeUnion,
    CodeAstNode,
    ContentAstNode,
    ExpressionAstNode,
    PassageAstNode,
    PlainTextAstNode
} from '@hyperthread/parser/ast-nodes';
import styled from 'styled-components';
import {RenderContext} from '@hyperthread/engine/story/render-context';

const PassageLink = styled.span`
  cursor: pointer;
  color: orange;
`;

const ParagraphSeparator = styled.div`
  height: 1.5rem;
`;

let uniqueId = 0;

function renderAst(node: AstNodeUnion, context: RenderContext): ReactNode
{
    try
    {
        switch (node.type)
        {
            case AstNodeType.Passage:
                return renderPassage(node, context);
            case AstNodeType.PlainText:
                return renderPlainText(node);
            case AstNodeType.Expression:
                return renderExpression(node, context);
            case AstNodeType.Code:
                return renderCode(node, context);
            case AstNodeType.Content:
                return renderContent(node, context);
            case AstNodeType.HardLineBreak:
                return renderHardLineBreak();
            case AstNodeType.ParagraphSeparator:
                return renderParagraphSeparator();
            default:
                return `Unknown node type '${node.type}'`;
        }
    }
    catch (err)
    {
        return renderError(err);
    }
}

export function renderPassage(node: PassageAstNode, context: RenderContext): ReactNode
{
    return renderContent(node.content, context);
}

function renderContent(node: ContentAstNode, context: RenderContext): ReactNode
{
    return node.children.map(child => renderAst(child, context));
}

function renderPlainText(node: PlainTextAstNode): ReactNode
{
    return node.content;
}

function renderHardLineBreak(): ReactNode
{
    return <br key={uniqueId++} />;
}

function renderParagraphSeparator(): ReactNode
{
    return <ParagraphSeparator key={uniqueId++} />;
}

function renderExpression(node: ExpressionAstNode, context: RenderContext): ReactNode
{
    switch (node.name)
    {
        case 'link':
        {
            // TODO validate parameter
            const evaluatedParameter = evaluateInContext(node.parameters[0], context);
            return <PassageLink
                key={uniqueId++}
                onClick={() => context.setState(draft => draft.passageName = evaluatedParameter)}
            >
                {node.content ? renderContent(node.content, context) : evaluatedParameter}
            </PassageLink>;
        }
        case 'back':
        {
            return <PassageLink
                key={uniqueId++}
                onClick={() => context.setState(draft => {
                    if (!context.history.canGoBack)
                        return;
                    
                    const previous = context.history.buffer.get(context.history.buffer.size - 2)!;
                    draft.passageName = previous.passageName;
                })}
            >
                {node.content ? renderContent(node.content, context) : 'Back'}
            </PassageLink>;                
        }
        case 'if':
        {
            // TODO validate parameter
            const evaluatedParameter = evaluateInContext(node.parameters[0], context);

            if (evaluatedParameter && node.content)
                return renderContent(node.content, context);

            return null;
        }
        default:
        {
            return <div key={uniqueId++} style={{ color: 'orange' }}>&#123;I'm a {node.name} expression!&#125;</div>;
        }
    }
}

// The whole function is evaled, because with is not allowed in strict mode
const evalFunc = new Function('scriptContent',`with(this) { return eval('"strict mode";' + scriptContent); }`);

function renderCode(node: CodeAstNode, context: RenderContext)
{
    const result = evaluateInContext(node.content, context);
    
    if (!node.isBlock)
    {
        switch (typeof result)
        {
            case 'string':
            case 'number':
                return result;
        }
    }
    
    return undefined;
}

function renderError(err: Error)
{
    console.error(err);
    return <div key={uniqueId++} style={{color: 'red'}}>{err.message}</div>;
}

function evaluateInContext(code: string, context: RenderContext): any
{
    return evalFunc.call(context.stateProxy, code);
}