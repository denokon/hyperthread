import {ConnectDragPreview, ConnectDragSource, DragSource} from 'react-dnd';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {default as React, useLayoutEffect} from 'react';
import {getEmptyImage} from 'react-dnd-html5-backend';
import {MapItem} from './map-item';

export interface DraggableMapItemProps
{
    passage: PassageInterface;
    onClick?(event: React.MouseEvent<HTMLDivElement>): void;

    // Collected props
    connectDragSource: ConnectDragSource;
    connectDragPreview?: ConnectDragPreview;
    isDragging?: boolean;
}

function DraggableMapItemComponent({connectDragSource, connectDragPreview, passage, isDragging, onClick}: DraggableMapItemProps)
{
    if (isDragging)
        return null;

    useLayoutEffect(() => {
        if (!connectDragPreview)
            return;

        // Use empty image as a drag preview so browsers don't draw it
        // and we can draw whatever we want on the custom drag layer instead.
        connectDragPreview(getEmptyImage());
    }, [connectDragPreview]);

    const style = {left: passage.gridPosition[0] + 'px', top: passage.gridPosition[1] + 'px'};

    return <MapItem passage={passage} style={style} onClick={onClick} ref={ref => connectDragSource(ref)} />;
}

export const DraggableMapItem = DragSource(
    'passage',
    {
        beginDrag({passage}: DraggableMapItemProps) {
            return {passage};
        }
    },
    (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    })
)(DraggableMapItemComponent);