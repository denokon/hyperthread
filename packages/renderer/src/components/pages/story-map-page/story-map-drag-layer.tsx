import {DragLayer, XYCoord} from 'react-dnd';
import * as React from 'react';
import {styled} from '../../../themes/theme';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {snapToGrid} from './snap-to-grid';
import {MapItem} from './map-item';
import {zIndexDragLayer} from '../../../themes/z-indexes';

const StoryMapDragLayerWrapper = styled.div`
  position: fixed;
  pointer-events: none;
  z-index: ${zIndexDragLayer};
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
`;

const DraggedStoryMapWrapper = styled.div``;

interface StoryMapDragLayerProps
{
    item?: { passage: PassageInterface };
    itemType?: string|symbol|null,
    initialOffset?: XYCoord|null,
    currentOffset?: XYCoord|null,
    isDragging?: boolean
}

function StoryMapDragLayerComponent({isDragging, item, initialOffset, currentOffset}: StoryMapDragLayerProps)
{
    if (!isDragging || !initialOffset || !currentOffset || !item)
        return null;

    let { x, y } = currentOffset;

    x -= initialOffset.x;
    y -= initialOffset.y;
    [x, y] = snapToGrid(x, y);
    x += initialOffset.x;
    y += initialOffset.y;
    
    return <StoryMapDragLayerWrapper>
        <DraggedStoryMapWrapper style={{ transform: `translate(${x}px, ${y}px)` }}>
            <MapItem passage={item.passage} />
        </DraggedStoryMapWrapper>
    </StoryMapDragLayerWrapper>;
}

export const StoryMapDragLayer = DragLayer(monitor => ({
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging()
}))(StoryMapDragLayerComponent);