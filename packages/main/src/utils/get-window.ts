import {RpcRequest} from '@epicentric/rxpc';
import {BrowserWindow} from 'electron';
import {ElectronMainTransport} from '@epicentric/rxpc-transport-electron';

export function getWindow(request: RpcRequest): BrowserWindow
{
    const webContents = (request.client!.transport as ElectronMainTransport).webContents;
    return BrowserWindow.fromWebContents(webContents);
}