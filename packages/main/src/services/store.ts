import {Observable, Subject} from 'rxjs';
import path from "path";
import {app} from 'electron';
//@ts-ignore
import PromiseThrottle from 'promise-throttle';
import {promises as fs} from "fs";
import msgpack from 'msgpack-lite';
import {startWith, switchMap} from 'rxjs/operators';

const promiseThrottle = new PromiseThrottle({
    requestsPerSecond: 1 / 5
});

export class Store
{
    private readonly filePath: string;
    private readonly loaded: Promise<void>;
    
    private data: { [key: string]: any } = {};
    private change: Subject<any> = new Subject;

    public constructor()
    {
        this.filePath = path.resolve(app.getPath('userData'), 'data.msgpack');
        this.loaded = this.loadFile();
        this.dump = this.dump.bind(this);

        this.change.subscribe(() => {
            promiseThrottle.add(this.dump);
        });
    }

    public async delete(key: string): Promise<void>
    {
        await this.loaded;
        delete this.data[key];
        this.change.next();
    }

    public get<T>(key: string, defaultValue?: T): Observable<T|undefined>
    {
        return this.change.pipe(
            startWith(null),
            switchMap(() => this.getPromise(key, defaultValue))
        );
    }

    public async getPromise<T>(key: string): Promise<T|undefined>
    public async getPromise<T>(key: string, defaultValue: T): Promise<T>
    public async getPromise<T>(key: string, defaultValue?: T): Promise<T|undefined>
    {
        await this.loaded;
        return key in this.data ? this.data[key] : defaultValue;
    }

    public async set<T>(key: string, value: T): Promise<void>
    {
        await this.loaded;
        this.data[key] = value;
        this.change.next();
    }

    private async loadFile()
    {
        try
        {
            const canRead = await fs.access(this.filePath).then(() => true, () => false);

            if (canRead)
            {
                const data = await fs.readFile(this.filePath);
                this.data = msgpack.decode(data);
            }
            else
            {
                this.data = {};
                await this.dump();
            }
        }
        catch (err)
        {
            this.data = {};
        }
    }

    private async dump()
    {
        const data = msgpack.encode(this.data);

        await fs.writeFile(this.filePath, data);
    }
}

export const store = new Store;