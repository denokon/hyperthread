import {Logger} from '@epicentric/logger';
import {BrowserOutputStream} from '@epicentric/logger-browser-output';

export const logger = new Logger();

logger.addOutput(new BrowserOutputStream());