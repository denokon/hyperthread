import {Configuration} from 'webpack';

import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// @ts-ignore
import CircularDependencyPlugin from 'circular-dependency-plugin';

import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import {TsconfigPathsPlugin} from 'tsconfig-paths-webpack-plugin';

// https://github.com/dividab/tsconfig-paths-webpack-plugin/issues/31#issuecomment-447655199
process.env['TS_NODE_PROJECT'] = '';

const distDir = path.join(__dirname, 'dist');
const distFile = 'index.js';

const config: Configuration = {
    target: 'web',
    
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        plugins: [new TsconfigPathsPlugin()],
        symlinks: false
    },
    
    entry: './src/index.tsx',
    devtool: 'source-map',
    mode: 'development',
    output: {
        path: distDir,
        filename: distFile,
        publicPath: '/'
    },
    devServer: {
        stats: 'minimal',
        historyApiFallback: {
            disableDotRule: true
        },
        // hot: true,
        hot: false,
        overlay: {
            warnings: true,
            errors: true
        }
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            poolRespawn: false
                        }
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            envName: 'development'
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: true
                        }
                    }
                ]
            },
            {
                test: /\.jsx$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            poolRespawn: false
                        }
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            envName: 'development'
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|svg|jpg|gif|woff2?|eot|ttf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        // new HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'Hyperthread',
            template: './src/index.html',
            inject: false
        }),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
};

export default config;