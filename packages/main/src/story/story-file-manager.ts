import {
    createEmptyStory,
    ParsedStoryInterface,
    StoryInterface
} from '@hyperthread/common/models/story-interface';
import {outputFile, readFile, remove} from 'fs-extra';
import {safeDump, safeLoad} from 'js-yaml';
import path from 'path';
import glob from 'fast-glob';
import {store} from '../services/store';
import {sanitizeFilename} from '../utils/file-utility';
import {parseToAst} from '@hyperthread/parser';
import {createEmptyPassage, generatePassageId, PassageInterface} from '@hyperthread/common/models/passage-interface';

export const storyFileExtension = 'htproj';
export const passageFileExtension = 'passage';

export class StoryFileManager
{
    public async open(storyPath: string): Promise<StoryInterface>
    {
        const storyMetadataRaw = await this.readConfigFile(storyPath);
        
        const story = createEmptyStory();
        story.name = storyMetadataRaw.name;
        story.path = storyPath;

        const storyDirectory = path.dirname(storyPath);
        const passagesDirectory = path.join(storyDirectory, 'passages');

        const passageFiles = await this.getPassageFilelist(passagesDirectory, true);

        const passages: PassageInterface[] = [];

        for (const passageFile of passageFiles)
        {
            const raw = await this.readConfigFile(passageFile);

            passages.push({
                id: raw.id,
                name: raw.name,
                content: raw.content,
                filename: path.basename(passageFile, path.extname(passageFile)),
                gridPosition: raw.gridPosition || [50, 50]
            });
        }
        
        if (!passages.length)
        {
            const defaultPassage = createEmptyPassage();
            defaultPassage.id = generatePassageId();
            passages.push(defaultPassage);
        }
        
        story.passages = passages;
        story.startPassageId = storyMetadataRaw.startPassageId || passages[0].id;

        try
        {
            const cacheFilePath = path.join(storyDirectory, '.cache.yml');
            const raw = await this.readConfigFile(cacheFilePath);

            story.openPassageId = raw.openPassageId || null;
        }
        catch (err)
        {
            console.error('Error while opening story cache', err);
        }

        return story;
    }
    
    public async save(story: StoryInterface): Promise<void>
    {
        const storyDirectory = path.dirname(story.path);
        
        const storyFileContent = safeDump({
            name: story.name,
            startPassageId: story.startPassageId
        });

        await outputFile(story.path, storyFileContent);
        
        const cacheFileContent = safeDump({
            openPassageId: story.openPassageId
        }, {
            skipInvalid: true // Prevent throwing on undefined
        });

        await outputFile(path.join(storyDirectory, '.cache.yml'), cacheFileContent);

        const recentStories = await store.getPromise<StoryInterface[]>('recentStories', []);
        const foundIndex = recentStories.findIndex(current => current.id === story.id);
        
        if (-1 < foundIndex)
            recentStories.splice(foundIndex, 1, story);
        else
            recentStories.push(story);

        const passagesDirectory = path.join(storyDirectory, 'passages');
        
        const writtenPassageFilenames: Set<string> = new Set;

        for (const passage of story.passages)
        {
            const basename = await this.computePassageFilename(passagesDirectory, passage.name, passage.filename);
            const filename = basename + '.' + passageFileExtension;

            const passageFileContent = safeDump({
                name: passage.name,
                content: passage.content,
                gridPosition: passage.gridPosition
            });

            await outputFile(path.join(passagesDirectory, filename), passageFileContent);
            
            writtenPassageFilenames.add(filename);
        }

        const passageFiles = await this.getPassageFilelist(passagesDirectory, false);
        
        for (const passageFile of passageFiles)
        {
            if (!writtenPassageFilenames.has(passageFile))
                await remove(path.join(passagesDirectory, passageFile));
        }

        await store.set('recentStories', recentStories);
    }

    public getStoryAsJson(story: StoryInterface): string
    {
        return JSON.stringify({
            name: story.name,
            startPassageId: story.startPassageId,
            passages: story.passages.map(passage => ({
                name: passage.name,
                node: parseToAst(passage.content).ast || { type: 'Passage', children: [] }
            }))
        });
    }

    public getStoryAsParsedStory(story: StoryInterface): ParsedStoryInterface
    {
        return {
            id: story.path,
            name: story.name,
            startPassageId: story.startPassageId,
            passages: story.passages.map(passage => ({
                name: passage.name,
                node: parseToAst(passage.content).ast || { type: 'Passage', children: [] },
            })) as any // TODO fix this
        };
    }
    
    private async readConfigFile(path: string): Promise<any>
    {
        const content = await readFile(path, { encoding: 'utf8' });
        return safeLoad(content, { filename: path });
    }

    private async computePassageFilename(baseDir: string, name: string, existingFilename?: string): Promise<string>
    {
        // Get sanitized filename based on passage title
        const potentialFilename = sanitizeFilename(name);

        // If there's an existing file, check if the passage's title still matches it. If it does, then just keep
        // using it. 
        if (existingFilename)
        {
            const existingFilenameNoPostfix = existingFilename.replace(/-\d+$/, '');

            if (existingFilenameNoPostfix === potentialFilename)
                return existingFilename;
        }

        // Get all passage files with the potential filename as their name (with or without postfix)
        const existingFiles: string[] = await glob(`${potentialFilename}*`, {
            cwd: baseDir,
            onlyFiles: true,
            absolute: true
        });

        // If there's no such file, just take the potential name
        if (!existingFiles.length)
            return potentialFilename;

        // Sort the names, so everything is in place for the following for-loop
        existingFiles.sort();

        for (let i = 0; i < existingFiles.length; i++)
        {
            // The first entry should be the one without a postfix
            if (0 === i)
            {
                if (existingFiles[i] !== potentialFilename)
                    return potentialFilename;
            }
            // The later entries all have the appropriate postfixes. Find the first one that doesn't exist
            else
            {
                const currentPotentialFilename = potentialFilename + '-' + i;

                if (existingFiles[i] !== currentPotentialFilename)
                    return currentPotentialFilename;
            }
        }

        // If we're here, we have to take the next postfix that doesn't exist yet
        return potentialFilename + '-' + existingFiles.length;
    }
    
    private getPassageFilelist(passagesDirectory: string, absolute: boolean): Promise<string[]>
    {
        return glob(`*.${passageFileExtension}`, {
            cwd: passagesDirectory,
            onlyFiles: true,
            absolute
        });
    }
}

export const storyManager = new StoryFileManager;