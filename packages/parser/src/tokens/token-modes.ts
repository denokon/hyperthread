// These are here so tokens can push modes without the file getting in to a circular reference

export const defaultMode = 'defaultMode';
export const expressionMode = 'expressionMode';
export const inlineCodeMode = 'inlineCodeMode';
export const blockCodeMode = 'blockCodeMode';