import {RpcRequest} from '@epicentric/rxpc';

type Parameters<T> = T extends (... args: infer T) => any ? T : never;
// Use this instead of the one in utility-types, because this is more lenient when used on unknown keys
type ReturnType<T> = T extends (... args: any[]) => infer T ? T : never;

export type ServerInterface<T> = {
    // For each key in type T, map a function type that begins with the parameter "request: RpcRequest", then has the
    // same parameters as the original function type. Let the return type be the same as the original function type
    // OR if the original return type was a Promise, then allow the unboxed Promise type to be the return value as
    // well.
    [P in keyof T]: (request: RpcRequest, ...args: Parameters<T[P]>) =>
        ReturnType<T[P]> | (ReturnType<T[P]> extends Promise<infer TPromise> ? TPromise : never);
}