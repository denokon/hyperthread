import {createGlobalStyle} from '../themes/theme';
import {interFontMixin} from '@hyperthread/common/inter-font-mixin';
import {Play} from 'styled-icons/fa-solid';

export const GlobalStyle = createGlobalStyle`
  ${interFontMixin}
  
  .tippy-tooltip {
    filter: drop-shadow(0 0 .3rem #999);
    padding: 1.2rem;
    max-width: none;
  }
  
  ${Play as any} {
    position: relative;
    left: .1em;
  }
`;