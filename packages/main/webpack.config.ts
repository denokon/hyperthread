import {Configuration} from 'webpack';

import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// @ts-ignore
import CircularDependencyPlugin from 'circular-dependency-plugin';
import path from 'path';
// @ts-ignore
// import createElectronReloadWebpackPlugin from 'electron-reload-webpack-plugin';
// import CopyWebpackPlugin from 'copy-webpack-plugin';
import {TsconfigPathsPlugin} from 'tsconfig-paths-webpack-plugin';
const NodemonPlugin = require('nodemon-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

// https://github.com/dividab/tsconfig-paths-webpack-plugin/issues/31#issuecomment-447655199
process.env['TS_NODE_PROJECT'] = '';

const distDir = path.join(__dirname, 'dist');
const distFile = 'main.js';

// const ElectronReloadWebpackPlugin = createElectronReloadWebpackPlugin({
//     path: path.resolve(distDir, distFile),
//     logLevel: 2
// });

const config: Configuration = {
    target: 'node',

    resolve: {
        extensions: ['.ts', '.js'],
        symlinks: false,
        plugins: [new TsconfigPathsPlugin()],
    },
    
    stats: 'minimal',

    externals: [
        nodeExternals({
            whitelist: [
                /^@hyperthread\//,
                /^@epicentric\//,
                /^lodash-es/
            ]
        }),
        nodeExternals({
            modulesDir: path.join(__dirname, '../../node_modules'),
            whitelist: [
                /^@hyperthread\//,
                /^@epicentric\//,
                /^lodash-es/
            ]
        })
    ],
    
    entry: './src/index.ts',
    devtool: 'source-map',
    mode: 'development',
    output: {
        path: distDir,
        filename: distFile
    },
    
    node: false,

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            poolRespawn: false
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            // transpileOnly: true,
                            happyPackMode: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|gif|woff2?|eot|ttf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: 'dist'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        // ElectronReloadWebpackPlugin(),
        // new CopyWebpackPlugin([
        //     'package.json'
        // ]),
        new NodemonPlugin({
            exec: ['electron', '-r', 'source-map-support/register', /*'--trace-deprecation',*/ '--inspect'/*, '--inspect-brk'*/ ],
            script: './dist/main.js',
            env: {
                'ELECTRON_DISABLE_SECURITY_WARNINGS': '1'
            },
            // cwd: path.resolve(__dirname, 'dist')
        }),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
};

export default config;