export const uiControllerPromiseFunctions = [ 'getDefaultCreatePath', 'browseDirectory' ];

export interface UiControllerInterface
{
    getDefaultCreatePath(): Promise<string>;
    browseDirectory(title: string, startingPath?: string): Promise<string|null>;
    minimize(): Promise<void>;
    close(): Promise<void>;
}