import {Observable} from 'rxjs';

export interface GetParams<T>
{
    key: string;
    defaultValue?: T;
}

export interface SetParams<T>
{
    key: string;
    value: T;
}

export interface DeleteParams
{
    key: string;
}

export interface StoreInterface
{
    get<T>(params: GetParams<T>): Observable<T|undefined>;
    set<T>(params: SetParams<T>): Observable<void>;
    delete(params: DeleteParams): Observable<void>;
}