import Modal, {ModalProps} from 'styled-react-modal';
import {PassageEditor} from '../../editor/passage-editor';
import {StoryInterface} from '@hyperthread/common/models/story-interface';
import {LoadedStory} from '../../../loaded-story';
import {ComponentClass, default as React, useCallback, useMemo, useRef, useState} from 'react';
import {history} from '../../../history';
import {delay} from '@epicentric/utilbelt';
import {StyledComponent} from 'styled-components';
import {css} from '../../../themes/theme';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {transitions} from 'polished';
import {zIndexModal} from '../../../themes/z-indexes';
import {TitleBarHeight} from '../../title-bar';

// Written separately for syntax highlighting
const StyledEditorModalStyle = css<{ isOpenStyle: boolean }>`
    display: flex;
    padding: calc(1rem + ${TitleBarHeight}) 0 1rem;
    height: 100%;
    width: 70%;
    opacity: ${props => props.isOpenStyle ? 1 : 0};
    transform: ${props => props.isOpenStyle ? 'none' : 'translateY(3rem)'};
    ${transitions(['opacity', 'transform'], 'ease-in-out .25s')};
    z-index: ${zIndexModal};    
`;

const StyledEditorModal = Modal.styled`${StyledEditorModalStyle}` as unknown as StyledComponent<ComponentClass<ModalProps & { isOpenStyle: boolean }>, {}>;

interface EditorModalProps
{
    story: StoryInterface;
    loadedStory: LoadedStory;
    storyId: string;
    passageId?: string;
}

export function EditorModal({story, loadedStory, storyId, passageId}: EditorModalProps)
{
    const lastPassage = useRef<PassageInterface|null>(null);
    const [isOpenStyle, setIsOpenStyle] = useState(false);
    
    const onDismissModal = useCallback(() => {
        history.push(`/stories/${storyId}`);
    }, []);
    
    const onBeforeClose = useCallback(async () => {
        setIsOpenStyle(false);
        
        await delay(250);
        
        // Release memory after animation
        setTimeout(() => lastPassage.current = null, 0);
    }, []);

    const activePassageIndex = useMemo(() => {
        if (!passageId)
            return -1;

        return story.passages.findIndex(passage => passage.id === passageId);
    }, [story, passageId]);

    const activePassage = story.passages[activePassageIndex];

    const onAfterOpen = useCallback(async () => {
        await delay(0);

        lastPassage.current = activePassage;
        setIsOpenStyle(true);
    }, [activePassage]);
    
    const onPassageChange = useCallback((passage: PassageInterface) => {
        loadedStory.updatePassage(passage)
    }, []);

    const displayPassage = activePassage || lastPassage.current;
    
    const onPassageDelete = useCallback(() => {
        loadedStory.deletePassage(displayPassage);
    }, [displayPassage]);

    return <StyledEditorModal
        isOpen={!!activePassage}
        afterOpen={onAfterOpen}
        beforeClose={onBeforeClose}
        onBackgroundClick={onDismissModal}
        onEscapeKeydown={onDismissModal}
        isOpenStyle={isOpenStyle}
        backgroundProps={{ isOpenStyle }}
    >
        {displayPassage && <PassageEditor
            passage={displayPassage}
            onChange={onPassageChange}
            onDelete={onPassageDelete}
        />}        
    </StyledEditorModal>;
}