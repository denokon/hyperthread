import {BaseComponent, bind, tracked} from '@epicentric/utilbelt-react';
import {RouteComponentProps} from 'react-router';
import {createRef, default as React, ReactNode, RefObject} from 'react';
import {TitleBar} from '../title-bar';
import {ParsedStoryInterface} from '@hyperthread/common/models/story-interface';
import {stringDecode, stringEncode} from '@hyperthread/common/string-encode';
import {filter, switchMap, takeUntil} from 'rxjs/operators';
import {fromEvent} from 'rxjs';
import {tap} from 'rxjs/internal/operators/tap';
import {styled} from '../../themes/theme';
import {PlainButton} from '../button';
import {Reset} from 'styled-icons/boxicons-regular';
import {ArrowCircleLeft, ArrowCircleRight} from 'styled-icons/fa-solid';
import {Page} from '../page';
import {PostmessageType} from '@epicentric/rxpc-transport-postmessage';
import {storyInterface} from '../../services/rpc-client';
import {PreviewServer} from '../../preview/preview-server';

const StyledPreviewPage = styled(Page)`
  display: flex;
  align-items: stretch;
  justify-content: stretch;
`;

const PreviewIframe = styled.iframe`
  border: none;
  flex-grow: 1;
`;

interface PreviewPageProps extends RouteComponentProps<{ path: string }>
{

}

export class PreviewPage extends BaseComponent<PreviewPageProps>
{
    @tracked
    private story: ParsedStoryInterface|null = null;
    @tracked
    private server: PreviewServer|null = null;
    
    private iframeRef: RefObject<HTMLIFrameElement> = createRef();

    public constructor(props: PreviewPageProps, context: {})
    {
        super(props, context);

        this.addObservable('story', this.didChange.pipe(
            filter(({prevProps, props}) => !prevProps.match
                || prevProps.match.params.path !== props.match.params.path
            ),
            switchMap(({props}) => {
                const path = stringDecode(props.match.params.path);
                return storyInterface.watch(path);
            }),
            tap(story => this.server && this.server.setStory(story))
        ));

        fromEvent<MessageEvent>(window, 'message').pipe(
            filter(event => {
                if (!this.iframeRef.current)
                    return false;
                
                if (event.source !== this.iframeRef.current.contentWindow)
                    return false;
                
                return event.data && event.data.type === PostmessageType.Connect;
            }),
            takeUntil(this.willUnmount)
        ).subscribe(event => {
            if (this.server)
                this.server.close(false);
            
            this.server = new PreviewServer(event.source as Window);
            
            if (this.story)
                this.server.setStory(this.story);
        });
    }
    
    public render(): ReactNode
    {
        return <StyledPreviewPage>
            <TitleBar
                isStartPage
                extraButtons={<>
                    <PlainButton
                        disabled={this.getCanGoBack()}
                        onClick={() => this.server && this.server.controller.goBack()}
                    >
                        <ArrowCircleLeft />
                    </PlainButton>
                    <PlainButton
                        disabled={this.getCanGoForward()}
                        onClick={() => this.server && this.server.controller.goForward()}
                    >
                        <ArrowCircleRight />
                    </PlainButton>
                    <PlainButton
                        onClick={this.onReset}
                        style={{color: 'red', fontSize: '1.4em'}}
                    >
                        <Reset />
                    </PlainButton>
                </>}
            >
                Preview: {this.story ? this.story.name : stringDecode(this.props.match.params.path)}
            </TitleBar>
            {this.story && <PreviewIframe
                ref={this.iframeRef}
                sandbox="allow-scripts allow-same-origin"
                src={'http://localhost:8081/?storyId=' + stringEncode(this.story.id)}
            />}
        </StyledPreviewPage>;
    }
    
    private getCanGoBack(): boolean
    {
        if (!this.server)
            return false;
        
        return 0 < this.server.historyIndex;
    }
    
    private getCanGoForward(): boolean
    {
        if (!this.server)
            return false;
        
        return this.server.historyIndex < this.server.historyLength - 1;
    }
    
    @bind
    private onReset()
    {
        if (!this.server)
            return;
        
        if (confirm('Are you sure?'))
            this.server.controller.reset();
    }
}