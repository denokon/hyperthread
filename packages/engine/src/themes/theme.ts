import * as baseStyled from 'styled-components';
import {ThemedStyledComponentsModule} from 'styled-components';
import {BaseTheme} from '@hyperthread/common/base-theme';

export interface Theme extends BaseTheme
{
}

// Based on: https://gist.github.com/chrislopresto/490ef5692621177ac71c424543702bbb
const { default: styled, ThemeProvider, withTheme, createGlobalStyle, css } = baseStyled as ThemedStyledComponentsModule<Theme>;

export { styled, ThemeProvider, withTheme, createGlobalStyle, css };