import {styled} from '../../../themes/theme';
import {TitleBarHeight} from '../../title-bar';
import {ConnectDropTarget, DropTarget, DropTargetMonitor} from 'react-dnd';
import * as React from 'react';
import {LoadedStory} from '../../../loaded-story';
import produce from 'immer';
import {PassageInterface} from '@hyperthread/common/models/passage-interface';
import {DraggableMapItem} from './draggable-map-item';
import {snapToGrid, snapToGridSize} from './snap-to-grid';
import {rem} from 'polished';

const backgroundSize = rem(snapToGridSize, 10);
const backgroundSizeLarge = rem(snapToGridSize * 4, 10);

const StyledMapArea = styled.div`
  min-width: 100%;
  min-height: 100%;
  padding-top: ${TitleBarHeight};
  background-color: #010c18;
  background-image: linear-gradient(90deg,#031830 1px,transparent 0),linear-gradient(180deg,#031830 1px,transparent 0),linear-gradient(90deg,#021122 1px,transparent 0),linear-gradient(180deg,#021122 1px,transparent 0);
  background-size: ${backgroundSizeLarge} ${backgroundSizeLarge}, ${backgroundSizeLarge} ${backgroundSizeLarge}, ${backgroundSize} ${backgroundSize}, ${backgroundSize} ${backgroundSize};
`;

interface MapAreaProps
{
    connectDropTarget: ConnectDropTarget;
    passages: PassageInterface[];
    story: LoadedStory;
    onPassageChange(passage: PassageInterface): void;
    onPassageClick(passage: PassageInterface): void;
}

export function MapAreaComponent({connectDropTarget, passages, onPassageClick}: MapAreaProps)
{
    return <StyledMapArea ref={ref => connectDropTarget(ref)}>
        {passages.map(passage => <DraggableMapItem
            key={passage.name}
            passage={passage}
            onClick={() => onPassageClick(passage)}
        />)}
    </StyledMapArea>;
}

export const MapArea = DropTarget(
    'passage',
    {
        drop(props: MapAreaProps, monitor: DropTargetMonitor) {            
            const { passage }: { passage: PassageInterface } = monitor.getItem();
            const delta = monitor.getDifferenceFromInitialOffset()!;
            
            const left = Math.round(passage.gridPosition[0] + delta.x);
            const top = Math.round(passage.gridPosition[1] + delta.y); 
            
            let newGridPosition: [number, number] = snapToGrid(left, top);
            
            props.onPassageChange(produce(passage, draft => {
                draft.gridPosition = newGridPosition;
            }));
        }
    },
    (connect) => ({
        connectDropTarget: connect.dropTarget()
    })
)(MapAreaComponent);