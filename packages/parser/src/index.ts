import {lexer} from './lexer';
import {parser} from '@hyperthread/parser/parser';
import {visitor} from '@hyperthread/parser/visitor';

export function tokenize(passageText: string)
{
    return lexer.tokenize(passageText);
}

export function parseToCst(passageText: string)
{
    const lexingResult = tokenize(passageText);
    // setting a new input will RESET the parser instance's state.
    parser.input = lexingResult.tokens;
    // any top level rule may be used as an entry point
    const cst = parser.passage();
    
    return {
        cst,
        lexingResult,
        parseErrors: parser.errors
    };
}

export function parseToAst(passageText: string)
{
    const results = parseToCst(passageText);
    
    const ast = parser.errors.length ? null : visitor.visit(results.cst);
    
    return {
        ...results,
        ast
    };
}