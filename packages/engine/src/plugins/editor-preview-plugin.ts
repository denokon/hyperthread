import {PlaySession} from '@hyperthread/engine/story/play-session';
import {ServerInterface} from '@hyperthread/common/rpc-interfaces/server-interface';
import {CurrentIndex, PreviewClientInterface} from '@hyperthread/common/rpc-interfaces/preview-client-interface';
import {BrowserIdGenerator} from '@epicentric/binary-id';
import {MsgpackRpcEncoder, rpcCallable, RpcClient, RpcHandler, RpcRequest} from '@epicentric/rxpc';
import {PostmessageTransport} from '@epicentric/rxpc-transport-postmessage';
import {PreviewServerInterface} from '@hyperthread/common/rpc-interfaces/preview-server-interface';
import {map} from 'rxjs/operators';
import {Observable, ReplaySubject} from 'rxjs';
import {StateInterface} from '@hyperthread/engine/story/state';
import {RenderContext} from '@hyperthread/engine/story/render-context';
import {mapKeys} from 'lodash-es';

const rpcHandler = new RpcHandler;
const rpcHook = rpcHandler.hooks.hook;

const encoder = new MsgpackRpcEncoder;
const idGenerator = new BrowserIdGenerator;

class PreviewClient implements ServerInterface<PreviewClientInterface>
{
    public readonly controller: PreviewServerInterface;
    
    private readonly client: RpcClient;
    private readonly lastState$: ReplaySubject<StateInterface> = new ReplaySubject(1);
    
    public constructor(private readonly session: PlaySession)
    {
        rpcHandler.registerClass('previewClient', this);
        
        const transport = new PostmessageTransport({ targetWindow: window.parent, currentWindow: window });

        this.client = new RpcClient({
            encoder,
            idGenerator,
            transport,
            pingEnabled: false
        });
        
        this.client.message.subscribe(message => rpcHandler.handle(this.client, message, {}));
        this.client.connect();

        this.controller = rpcHandler.createProxyClass<PreviewServerInterface>(this.client, 'previewServer',
            () => ({}));

        session.on('postRender', (context: RenderContext) => this.lastState$.next({
            ...mapKeys(context.global, (value, key) => '$' + key),
            ...context.local
        }));
        
        this.controller.getStory().subscribe(story => session.setStory(story));
    }

    @rpcCallable()
    public currentIndex(request: RpcRequest): Observable<CurrentIndex>
    {
        const history = this.session.history;
        
        return history.current$.pipe(
            map(() => ({
                index: history.currentIndex,
                length: history.buffer.size
            }))
        );
    }

    @rpcCallable()
    public currentVariables(request: RpcRequest): Observable<StateInterface>
    {
        return this.lastState$;
    }
    
    @rpcCallable()
    public goBack(request: RpcRequest): void
    {
        this.session.history.back();
    }
    
    @rpcCallable()
    public goForward(request: RpcRequest): void
    {
        this.session.history.forward();
    }
    
    @rpcCallable()
    public reset(request: RpcRequest): void
    {
        this.session.history.clear();
    }
    
    public close(): void
    {
        this.client.transport.close();
    }
}

export function editorPreviewPlugin(session: PlaySession)
{
    // Put the instance somewhere, so it doesn't get garbage collected
    (session as any).__previewClient = new PreviewClient(session);
}