import {Observable} from 'rxjs';
import {ParsedStoryInterface} from '@hyperthread/common/models/story-interface';

export interface PreviewServerInterface
{
    getStory(): Observable<ParsedStoryInterface>;
}