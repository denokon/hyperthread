import {Observable} from 'rxjs';
import {StateInterface} from '@hyperthread/engine/story/state';

export interface CurrentIndex
{
    index: number;
    length: number;
}

export interface PreviewClientInterface
{
    currentIndex(): Observable<CurrentIndex>;
    reset(): Promise<void>;
    goBack(): Promise<void>;
    goForward(): Promise<void>;
    currentVariables(): Observable<StateInterface>;
}