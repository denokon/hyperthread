import {createGlobalStyle} from '@hyperthread/renderer/src/themes/theme';
import {baseGlobalStyle} from '@hyperthread/common/base-theme';
import {interFontMixin} from '@hyperthread/common/inter-font-mixin';

export const GlobalStyle = createGlobalStyle`
  ${baseGlobalStyle}
  ${interFontMixin}
  
  body {
    overflow-x: hidden;
    overflow-y: auto;
  }
`;