import {createToken} from 'chevrotain';

const codeStartChars = [];
const backtickCharCode = '`'.charCodeAt(0);

for (let i = 1; i < backtickCharCode; i++)
{
    codeStartChars.push(i);
}

for (let i = backtickCharCode + 1; i < 65535; i++)
{
    codeStartChars.push(i);
}

export const InlineCode = createToken({
    name: 'InlineCode',
    pattern: /[^`]+/, // Translation: match until a non escaped backtick is found
});

export const ClosingBacktick = createToken({
    name: 'ClosingBacktick',
    pattern: /`/,
    pop_mode: true
});

export const inlineCodeTokens = [
    InlineCode,
    ClosingBacktick
];

export const BlockCode = createToken({
    name: 'BlockCode',
    pattern: /(?:(?!```)[\s\S])+/, // Translation: match until a triple backtick is found
    line_breaks: true,
    start_chars_hint: codeStartChars
});

export const ClosingTripleBacktick = createToken({
    name: 'ClosingTripleBacktick',
    pattern: /```/,
    pop_mode: true
});

export const blockCodeTokens = [
    BlockCode,
    ClosingTripleBacktick
];