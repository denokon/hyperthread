import {MsgpackRpcEncoder, RpcClient, RpcEncoder, RpcHandler} from '@epicentric/rxpc';
import {ipcMain} from 'electron';
import {take} from 'rxjs/operators';
import {ElectronMainTransport} from '@epicentric/rxpc-transport-electron';
import {BinaryId, NodeIdGenerator} from '@epicentric/binary-id';
import WebContents = Electron.WebContents;
import IpcMainEvent = Electron.IpcMainEvent;

export class RpcServer
{
    private handler: RpcHandler|null = null;
    private clients: Map<WebContents, RpcClient> = new Map;
    private idGenerator = new NodeIdGenerator();
    private encoder: RpcEncoder = new MsgpackRpcEncoder(); 
    
    public initialize(handler: RpcHandler)
    {
        this.handler = handler;
        ipcMain.on('rpcConnect', (event: IpcMainEvent, id: BinaryId|Buffer) => {
            const webContents = event.sender;

            const existingClient = this.clients.get(webContents);

            if (existingClient)
            {
                const existingTransport = existingClient.transport as ElectronMainTransport;
                
                // Don't send out a disconnect message in the middle of another connection. It would trip it up.
                existingTransport.sendingEnabled = false;
                existingTransport.close();
            }
            
            const transport = new ElectronMainTransport({
                webContents,
                id: id instanceof Buffer ? new DataView(id.buffer) : id
            });
            
            const client = new RpcClient({
                idGenerator: this.idGenerator,
                encoder: this.encoder,
                transport,
                pingEnabled: false,
                reconnectionEnabled: false
            });
            
            client.message.subscribe(message => this.handler!.handle(client, message, {}));
                        
            client.connect();
            
            
            client.transport.closed
                .pipe(take(1))
                .subscribe(() => this.clients.delete(webContents));
            
            this.clients.set(webContents, client);
        });
    }
}

export const rpcServer = new RpcServer;