/// <reference path="../../../common/src/types/images.d.ts" />

import {
    CreateStoryParams,
    StoryControllerInterface
} from '@hyperthread/common/rpc-interfaces/story-controller-interface';
import {
    createEmptyStory,
    generateStoryId,
    ParsedStoryInterface,
    StoryInterface
} from '@hyperthread/common/models/story-interface';
import {rpcCallable, RpcRequest} from '@epicentric/rxpc';
import path from 'path';
import {ServerInterface} from '@hyperthread/common/rpc-interfaces/server-interface';
import {storyFileExtension, storyManager} from '../story/story-file-manager';
import {sanitizeFilename} from '../utils/file-utility';
import {BrowserWindow} from 'electron';
import {stringEncode} from '@hyperthread/common/string-encode';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {getWindow} from '../utils/get-window';
import Webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';

import webpackConfig from '../webpack-build/webpack.config';
import fs from 'fs-extra';
import {file} from 'tmp-promise';
import appIcon from '@hyperthread/common/icons/app-icon.png';
import {store} from '../services/store';

const webpackDevServerOptions = { ...webpackConfig.devServer };

interface PreviewWindow
{
    webpackCompiler: Webpack.Compiler;
    webpackDevServer: WebpackDevServer;
    window: BrowserWindow;
    story$: BehaviorSubject<StoryInterface>;
}

export class StoryController implements ServerInterface<StoryControllerInterface>
{
    public previewWindows: Map<string, PreviewWindow> = new Map();

    @rpcCallable()
    public async create(request: RpcRequest, params: CreateStoryParams): Promise<StoryInterface>
    {
        const path = await this.createFilePath(request, params.path, params.name, params.createDirectory);
        const story = createEmptyStory();
        story.id = generateStoryId();
        story.path = path;
        story.name = params.name;

        await this.save(request, story);
        
        await store.set('lastCreatePath', params.path);
        
        return story;
    }
    
    @rpcCallable()
    public openPath(request: RpcRequest, path: string): Promise<StoryInterface>
    {
        return storyManager.open(path);
    }  
    
    @rpcCallable()
    public async openId(request: RpcRequest, id: string): Promise<StoryInterface>
    {
        const recentStories = await store.getPromise<StoryInterface[]>('recentStories', []);
        
        const storyEntry = recentStories.find(story => story.id === id);
        
        if (!storyEntry)
            throw new Error(`Unknown story with id '${id}'`);
        
        return storyManager.open(storyEntry.path);
    }

    @rpcCallable()
    public save(request: RpcRequest, story: StoryInterface): Promise<void>
    {
        const previewWindow = this.previewWindows.get(story.path);
        
        if (previewWindow)
            previewWindow.story$.next(story);
        
        return storyManager.save(story);
    }
    
    @rpcCallable()
    public watch(request: RpcRequest, storyPath: string): Observable<ParsedStoryInterface>
    {
        const previewWindow = this.previewWindows.get(storyPath);
        
        if (!previewWindow)
            throw new Error(`Story '${storyPath}' is not being previewed`);
        
        return previewWindow.story$.pipe(
            map(story => storyManager.getStoryAsParsedStory(story))
        );
    }

    @rpcCallable()
    public createFilePath(request: RpcRequest, projectPath: string, storyName: string, createDirectory: boolean): string
    {
        const sanitizedStoryName = sanitizeFilename(storyName);
        const storyFilename = `${sanitizedStoryName}.${storyFileExtension}`;

        let finalPath: string;
        if (createDirectory)
        {
            finalPath = path.resolve(projectPath, sanitizedStoryName, storyFilename);
        }
        else
        {
            finalPath = path.resolve(projectPath, storyFilename);
        }
        
        return finalPath;
    }

    @rpcCallable()
    public async play(request: RpcRequest, story: StoryInterface): Promise<void>
    {
        const storyPath = story.path;
        
        if (this.previewWindows.has(storyPath))
            return;
        
        const {fd, path: entryPath, cleanup} = await file({
            postfix: '.tsx'
        });
        await fs.writeFile(fd, `import {PlaySession, editorPreviewPlugin, persistenceLocalforagePlugin, randomPlugin, DefaultTemplate} from '@hyperthread/engine';
import React from 'react';
import {render} from 'react-dom';

const playSession = new PlaySession({ id: '${story.path}', name: '${story.name}', passages: [] }, [
    editorPreviewPlugin,
    persistenceLocalforagePlugin,
    randomPlugin
]);

const appElement = document.createElement('div');
render(<DefaultTemplate session={playSession} />, document.body.appendChild(appElement));  
`);
        
        const currentWebpackConfig = { ...webpackConfig, entry: entryPath };

        const webpackCompiler = Webpack(currentWebpackConfig);
        
        const webpackDevServer = new WebpackDevServer(webpackCompiler, webpackDevServerOptions);
        
        await new Promise((resolve, reject) => webpackDevServer.listen(8081, '127.0.0.1', err => err ? reject(err) : resolve()));
        
        const window = new BrowserWindow({
            height: 600,
            width: 800,
            webPreferences: {
                nodeIntegration: true
            },
            frame: false,
            parent: getWindow(request),
            icon: appIcon
        });
        
        this.previewWindows.set(storyPath, {
            webpackCompiler,
            webpackDevServer,
            window,
            story$: new BehaviorSubject(story)
        });

        window.setMenu(null);
        window.on("closed", async () => {
            this.previewWindows.delete(storyPath);
            await new Promise(resolve => webpackDevServer.close(resolve));
            cleanup();            
        });
        
        window.loadURL('http://localhost:8080/preview/' + stringEncode(storyPath));
    }
}

export const storyController = new StoryController();