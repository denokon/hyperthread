import {ParsedStoryInterface, ParsedStoryPassage} from '@hyperthread/common/models/story-interface';
import React from 'react';
import {StateHistory} from '@hyperthread/engine/story/state-history';
import {RenderContext} from '@hyperthread/engine/story/render-context';
import {Immer, ImmerState} from 'immer';
import {StateInterface} from '@hyperthread/engine/story/state';
import {EventEmitter} from '@hyperthread/engine/story/event-emitter';

type PluginFunction = (session: PlaySession) => void;

const storyImmer = new Immer({
    autoFreeze: true,
    useProxies: true,/*
    onCopy(state: ImmerState) {
        console.log('onCopy', { ...state });
    },
    onAssign(state: ImmerState, prop: PropertyKey, value: any) {
        console.log('onAssign', {...state}, prop, value);
    },
    onDelete(state: ImmerState, prop: PropertyKey) {
        console.log('onDelete', {...state}, prop);
    }*/
});

export class PlaySession extends EventEmitter
{
    public readonly history: StateHistory = new StateHistory(20);

    public constructor(public story: ParsedStoryInterface, plugins: PluginFunction[])
    {
        super();
        
        this.history.current$.subscribe(state => {
            this.emit('historyPostPush', state);
        });
        
        this.initialize(plugins);
        
        this.setStory(story);
    }
    
    public async setStory(story: ParsedStoryInterface)
    {
        this.story = story;

        await this.emit('start');
        await this.emit('postStart');
    }
    
    public async render(renderFunc: (context: RenderContext) => void): Promise<RenderContext>
    {
        let context = new RenderContext(this, this.history.current);
        
        await this.emit('preRender', context);
        
        await this.renderContext(context, renderFunc);
        
        await this.emit('postRender', context);
        
        return context;
    }
    
    public async renderContext(context: RenderContext, renderFunc: (context: RenderContext) => void): Promise<void>
    {
        // noinspection UnnecessaryLocalVariableJS
        const global = context.global;
        
        // noinspection UnnecessaryLocalVariableJS
        const newGlobal = await storyImmer.produce(global, async draft => {
            context.global = draft; // Set the global to be mutable
            
            await this.emit('onRender', context);

            renderFunc(context);
        });
        
        context.global = newGlobal;
    }
    
    public async renderState(context: RenderContext, renderFunc: (state: StateInterface) => void): Promise<StateInterface>
    {
        await this.renderContext(context, draft => renderFunc(draft.global));
        
        return context.global;
    }

    private initialize(plugins: PluginFunction[]): void
    {
        for (const plugin of plugins)
        {
            plugin(this);
        }
    }
}

export function getStartPassage(story: ParsedStoryInterface): ParsedStoryPassage|null
{
    return story.passages.find(passage => passage.isStart) || null;
}

export function getPassage(story: ParsedStoryInterface, name: string|undefined): ParsedStoryPassage|null
{
    return story.passages.find(passage => name === passage.name) || null;
}