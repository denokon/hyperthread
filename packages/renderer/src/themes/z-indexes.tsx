export const zIndexTitleBar = 10;
export const zIndexDragLayer = 9;
export const zIndexModal = 3;
export const zIndexModalBackground = 2;